# Labyrinth
Tento program byl vytvořen pro účely týmového projektu z předmětu Seminář Javy (IJA) na Fakultě informačních technologií VUT v Brně v letním semestru 2015.

## Autoři
* Dita Cihlářová, xcihla02@stud.fit.vutbr.cz
* Martin Vondráček, xvondr20@stud.fit.vutbr.cz

## Zadání
Naším úkolem bylo implementovat program, který vychází z deskové hry Labyrinth.
Hra má tyto základní prvky:

* hrací deska
* hrací kameny
* úkolové karty
* 2 až 4 hráči reprezentovaní figurkami

### Hrací deska
Hrací deska je rozdělena na stejně velká čtvercová políčka. Hráč má možnost zvolit velikost hrací desky: 5x5, 7x7, 9x9 nebo 11x11 políček.

### Hrací kameny
Kamenů je vždy o jeden více než je počet políček hrací desky. Kámen může být právě jednoho ze tří typů: rovná cesta, zahnutá cesta (tvar písmene L), křižovatka (tvar písmene T). Poměr mezi počty kamenů různých typů je zhruba 1:1:1. Na náhodně vybraných kamenech jsou zobrazeny předměty, které hledají hráči.

Políčka v rozích hrací desky obsahují vždy kameny typu zahnutá cesta, políčka na zároveň lichém řádku a lichém sloupci vždy obsahují kameny typu křižovatka. Všechny cesty těchto pevných kamenů směřují do hrací desky, ne ven. Ostatní kameny se rozmístí náhodně. Jeden zůstane mimo hrací desku (takzvaný volný kámen).

### Úkolové karty
Obsahují předměty, které má hráč v labyrintu najít. Předmětů je pro jednu hru buď 12, nebo 24. Karty zatím nepřidělené hráčům jsou v balíčku, jehož obsah hráči nevidí.

### Hráči
Hráč je reprezentován figurkou unikátní barvy. Výchozí pozice figurky je v některém rohu hrací desky

## Princip hry
Hráči se snaží získat určitý počet předmětů (počet je daný poměrem počtu hráčů a počtu pokladů). Hledají vždy právě jeden, zadaný jejich úkolovou kartou. Získají ho tak, že se dostanou na hrací kámen, na kterém je předmět vyobrazen. Předmět tímto zmizí z labyrintu a uloží se hráči do inventáře. Hráč vyhrává v momentě, kdy získá poslední požadovaný předmět.

Tah jednoho hráče probíhá takto:

* Je mu přidělena úkolová karta, pokud žádnou nemá.
* Může otáčet volným kamenem pomocí tlačítka *rotate*.
* Musí vložit volný kámen na krajní políčko hrací desky (ale pouze v sudých řádcích a sloupcích), čímž posune celý řádek/sloupec. Kámen na opačném kraji se "vystčí" z hrací desky a stává se novým volným kamenem. Nelze vložit kámen tak, aby to vrátilo předcházející tah.
* Může se přesunout na nějaký jiný hrací kámen, ke kterému vede cesta.

### Navracení tahů - undo
V libovolné fázi hry je možné vrátit předchozí tah kliknutím na tlačítko *undo*. Tímto způsobem je možné postupně vrátit neomezené množství tahů až do stavu na začátku hry.

### Uložení a načtení rozehrané hry
Rozehranou hru je možné uložit do souboru prostřednictvím nabídky *Game* a volby *Save game*. Soubor je uložen ve stejném adresáři jako spustitelný soubor hry a je pojmenovám podle data a času uložení.

Název souboru má tedy tvar `yyyy-MM-dd_HH-mm.sav`.

Načtení uložené hry je možné z hlavní nabídky, ale i z právě spuštěné hry pomocí volby *Load game*.

## Klávesové zkratky
V hlavním herním okně, zobrazujícím hrací desku, je možné používat následující klávesové zkratky:

* `Ctrl` + `N` *New game*
* `Ctrl` + `S` *Save game*
* `Ctrl` + `L` *Load game*

## Screenshot
![screenshot](https://bytebucket.org/Wonderman2_12/vut-fit-ija-labyrinth/raw/9e8fad543685c7187fb69a9b5b077ce0230af427/examples/screenshot.png)

## Implementační informace
Pro GUI je použit JFC/Swing. Překlad a spuštění programu zajišťuje aplikace [ant](http://ant.apache.org/).
Projekt využívá návrhový vzor *MVC*. Navracení odehraných tahů (operace *undo*) je realizováno s využitím návrhového vzoru *Command*.

## Překlad
```sh
$  ant compile
```
Po překladu jsou *class* soubory v adresáři *build* a programová dokumentace v adresáři *doc*. 

Výsledný jar archiv `ija2015-client.jar` je v adresáři *dest-client*.

## Spuštění
```sh
$ ant run
```
Provede spuštění vytvořeného jar archivu.

## Dokumentace
Po překladu se programová dokumentace nachází v adresáři *doc*.

## Hodnocení
Dokončený projekt byl odevzdán v termínu a hodnocen 78 body z 80 možných.