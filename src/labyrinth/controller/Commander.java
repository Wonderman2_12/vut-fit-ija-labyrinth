/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 9. 5. 2015
 */


package labyrinth.controller;

import labyrinth.controller.game.ShiftCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * Commander zajistujici obsluhu prikazu aplikace.
 */
public class Commander{

    /**
     * Rozhrani prikazu aplikace.
     */
    public static interface Command {
        /** Vykonani prikazu */
        public void execute();
        /** Navraceni posledniho prikazu. Je mozne postupne navracet nekolik prikazu. */
        public void undo();
    }

    /**
     * Zajistuje zpracovani zadosti o vykonani nebo navraceni prikazu.
     */
    public static class Invoker {
        List<Command> commandsHistory = new ArrayList<>();

        /**
         * Vykona prikaz a ulozi ho do historie prikazu.
         * @param cmd prikaz k vykonani
         */
        public void execute(Command cmd) {
            commandsHistory.add(0, cmd);
            cmd.execute();
        }

        /**
         * Vrati posledni prikaz z historie prikazu.
         * Pokud je historie prikazu prazdna, nedela nic.
         */
        public void undo() {
            if (commandsHistory.size() > 0) {
                Command cmd = commandsHistory.remove(0);
                cmd.undo();
            }
        }


        /**
         * Vrati posledni vykonany prikaz vkladani kamene (shift).
         * Pokud jeste nebyl prikaz vkladani kamene vykonan (zacatek hry nebo po nacteni ulozene hry) vraci null.
         * @return posledni vykonany prikaz vkladani kamene (shift), null pokud takovy prikaz jeste nebyl vykonan
         */
        public ShiftCommand getLastShiftCommand(){
            for(int i = 0; i < commandsHistory.size(); i++){
                Command cmd = commandsHistory.get(i);
                if(cmd instanceof ShiftCommand){
                    return (ShiftCommand)cmd;
                }
            }
            return null;
        }
    }
}
