/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */

package labyrinth.controller.app;

import labyrinth.model.Game;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Ulozeni hry.
 */
public class SaveGameAction {

    /**
     * Ulozeni hry.
     * @param model modelova vrstva aplikace
     * @param frame okno aplikace, ze ktereho byla akce spustena
     */
    public void execute(Game model, JFrame frame){
        // zjistime datum a cas - to bude nazev ulozene hry
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
        String fileName = df.format(date);

        try{
            // otevreme bajtovy stream a soubor pro ulozeni hry, ulozime objekty, zavreme
            FileOutputStream saveFile = new FileOutputStream(fileName + ".sav");
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(model);
            save.close(); // toto zaroven zavre saveFile
        }
        catch(Exception exc){
            JOptionPane.showMessageDialog(frame, "Error occurred during saving game.");
            return;
        }
        JOptionPane.showMessageDialog(frame, "Game successfully saved in file:\n" + fileName + ".sav");
    }

}
