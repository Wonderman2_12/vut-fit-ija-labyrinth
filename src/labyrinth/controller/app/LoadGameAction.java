/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */

package labyrinth.controller.app;

import labyrinth.model.Game;
import labyrinth.view.GameFrame;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * Nacteni nove hry.
 */
public class LoadGameAction {
    /**
     * Nacte novou hru.
     * Pokud je prave rozehrana jina hra, pak ji zrusi.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     */
    public void execute(JFrame frame) {
        Game model;

        final JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File("."));

        int returnVal = fc.showOpenDialog(null);
        if(returnVal == JFileChooser.CANCEL_OPTION || returnVal == JFileChooser.ERROR_OPTION){
            //zavrit toto okno a vratit se do MainMenu
            return;
        }

        File chosenFile = fc.getSelectedFile();

        try{
            FileInputStream loadFile = new FileInputStream(chosenFile);
            ObjectInputStream load = new ObjectInputStream(loadFile);

            model = (Game) load.readObject();

            load.close();
        }
        catch(Exception exc){
            JOptionPane.showMessageDialog(frame, "Invalid file:\n" + chosenFile.getName());
            return;
        }

        frame.setVisible(false);
        frame.dispose();

        new GameFrame(model);
    }
}
