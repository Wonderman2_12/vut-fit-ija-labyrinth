/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.app;


import labyrinth.model.NewGameSettings;
import labyrinth.view.NewPlayersFrame;

import javax.swing.*;

/**
 * Vytvori okno pro zadani jmen hracu.
 */
public class CreateNewPlayersAction {

    /**
     * Zrusi okno a vytvori okno pro zadani jmen hracu.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     * @param settings nastaveni nove hry
     */
    public void execute(JFrame frame, NewGameSettings settings) {
        frame.setVisible(false);
        frame.dispose();
        new NewPlayersFrame(settings);
    }
}
