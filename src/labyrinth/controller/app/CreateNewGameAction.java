/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.app;


import labyrinth.model.Game;
import labyrinth.model.NewGameSettings;
import labyrinth.model.board.MazeBoard;
import labyrinth.model.player.Player;
import labyrinth.model.player.PlayerPool;
import labyrinth.model.treasure.CardPack;
import labyrinth.model.treasure.Treasure;
import labyrinth.view.GameFrame;

import javax.swing.*;

/**
 * Vytvori novou hru.
 */
public class CreateNewGameAction {

    /**
     * Zrusi okno a vytvori okno pro novou hru.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     * @param settings nastaveni nove hry
     */
    public void execute(JFrame frame, NewGameSettings settings) {
        frame.setVisible(false);
        frame.dispose();

        // vytvoreni modelu
        // vytvoreni desky
        MazeBoard mb = MazeBoard.createMazeBoard(settings.getMazeBoardSize());
        mb.newGame();
        // vytvoreni pokladu
        int treasuresNum = settings.getTreasuresNum();
        Treasure.createSet(treasuresNum);
        // vytvoreni ukolovych karet
        CardPack cp = new CardPack(treasuresNum, treasuresNum);
        cp.shuffle(); // zamichani karet
        // vytvoreni hracu
        PlayerPool pp = new PlayerPool();
        int goal = treasuresNum/settings.getPlayersNum();
        for(int i=0; i < settings.getNameArray().length && i < settings.getColorArray().length; i++) {
            pp.addPlayer(new Player(settings.getNameArray()[i],
                    settings.getColorArray()[i],
                    goal));
        }
        // umisteni figurek hracu na hraci desku
        mb.get(1, 1).getCard().addPlayer(pp.listPlayers().get(0)); // prvni hrac vlevo nahore
        mb.get(mb.size(), mb.size()).getCard().addPlayer(pp.listPlayers().get(1)); // druhy hrac vpravo dole
        if(pp.count() > 2) {
            mb.get(1, mb.size()).getCard().addPlayer(pp.listPlayers().get(2)); // treti hrac vpravo nahore
            if (pp.count() > 3){
                mb.get(mb.size(), 1).getCard().addPlayer(pp.listPlayers().get(3)); // ctvrty hrac vlevo dole
            }
        }

        // nahodne rozmisteni pokladu
        mb.setTreasuresRandomly();

        // prideleni uvodnich ukolovych karet
        for(int i = 0; i < pp.listPlayers().size(); i++){
            pp.listPlayers().get(i).setObjective(cp.popCard());
        }



        new GameFrame(new Game(mb, cp, pp));
    }
}
