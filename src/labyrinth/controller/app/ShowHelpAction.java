/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.app;

import javax.swing.*;


/**
 * Zobrazeni napovedy.
 */
public class ShowHelpAction {

    /**
     * Provede akci.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     */
    public void execute(JFrame frame){
        JOptionPane.showMessageDialog(frame,
            "Goal: to be the first who fills his whole inventory with treasures.\n\n" +
            "Turn phases:\n" +
            "1) Rotate and shift: You can rotate the spare stone shown in the left panel," +
            " then put it somewhere on the board. By putting the spare stone on some field," +
            " you shift that row or column.\n" +
            "You can not reverse last shift move done by previous player.\n" +
            "2) Move: Click on some accessible field to move your figure there." +
            " If there is a treasure you are currently searching for, you automatically" +
            " get it to your inventory.\n" +
            "3) Another player's turn.\n\n" +
            "Good luck and have fun!");
    }
}
