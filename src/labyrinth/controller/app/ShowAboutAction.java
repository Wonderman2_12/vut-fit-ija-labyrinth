/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.app;

import javax.swing.*;

/**
 * Zobrazi informace o aplikaci.
 */
public class ShowAboutAction {

    /**
     * Provede akci.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     */
    public void execute(JFrame frame){
        JOptionPane.showMessageDialog(frame,
                "VUT FIT IJA\n" +
                "Labyrinth\n" +
                "grp274\n" +
                "Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>\n" +
                "Martin Vondracek <xvondr20@stud.fit.vutbr.cz>\n" +
                "8. 5. 2015\n" +
                "\n" +
                "Treasure icons created by Teekatas Suwannakrua."
        );
    }
}
