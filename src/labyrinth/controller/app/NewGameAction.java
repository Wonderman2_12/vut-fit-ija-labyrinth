/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.app;


import labyrinth.view.NewGameMenuFrame;

import javax.swing.*;

/**
 * Zahaji proces vytvoreni nove hry.
 */
public class NewGameAction{

    /**
     * Zrusi okno a vytvori okno pro nastaveni nove hry.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     */
    public void execute(JFrame frame) {
        frame.setVisible(false);
        frame.dispose();
        new NewGameMenuFrame();
    }
}
