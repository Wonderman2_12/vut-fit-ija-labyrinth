/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.app;

import javax.swing.*;

/**
 * Zavreni okna a ukonceni aplikace.
 */
public class QuitAction {

    /**
     * Provede akci.
     * @param frame okno aplikace, ze ktereho byla akce spustena
     */
    public void execute(JFrame frame){
        frame.setVisible(false);
        frame.dispose();
        System.exit(0);
    }
}
