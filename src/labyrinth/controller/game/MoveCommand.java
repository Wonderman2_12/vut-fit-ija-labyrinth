/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.game;


import labyrinth.controller.Commander;
import labyrinth.model.Game;
import labyrinth.model.board.MazeField;
import labyrinth.model.player.Player;
import labyrinth.model.treasure.Treasure;
import labyrinth.model.treasure.TreasureCard;
import labyrinth.view.GameFrame;

import javax.swing.*;

/**
 * Presunuti zadaneho hrace na zadane policko.
 */
public class MoveCommand implements Commander.Command {
    private final Game model;
    private final GameFrame view;
    private final Player player;
    private Treasure PlayerFoundTreasure = null;
    private final MazeField fromMF;
    private final MazeField toMF;

    /**
     * Presune zadaneho hrace na zadane policko.
     * @param model modelova vrstva aplikace
     * @param view zobrazovaci vrstva aplikace
     * @param p hrac, ktery ma byt presunut
     * @param mf policko hraci desky, na ktere hrac kliknul, cil presunuti hrace
     */
    public MoveCommand(Game model, GameFrame view, Player p, MazeField mf) {
        this.model = model;
        this.view = view;
        this.player = p;
        this.PlayerFoundTreasure = null;
        this.fromMF = model.getMB().whereIs(p);
        this.toMF = mf;
    }

    @Override
    public void execute() {
        this.PlayerFoundTreasure = null;


        /*
         * Je mozne kliknout pouze na dosazitelne policko, proto zde jiz neni treba kontrolovat isReachable                 *
         */
        model.getMB().movePlayerTo(player, toMF);

        // kontroly po dokonceni presunu
        /*
         * aktualni pozice hrace po presunu (muze byt stejna jako pred presunem, pokud hrac kliknul na policko,
         * kde stal pred presunem
         */
        TreasureCard objective = player.getObjective();
        if(objective != null) {
            // hrac ma hledanou kartu
            Treasure objectiveTreasure = objective.getTr(); // hledany poklad
            // poklad na pozici hrace, pokud tam poklad neni bude localTreasure == null!
            Treasure localTreasure = toMF.getCard().getTreasure();
            if(objectiveTreasure.equals(localTreasure)) {
                // nalezen hledany poklad
                PlayerFoundTreasure = localTreasure;
                toMF.getCard().removeTrasure();
                player.moveObjectiveToInventory();
                if(player.getInventory().isCompleted()){
                    // hrac nasel posledni poklad potrebny k vitezstvi
                    // hrac je vitez
                    model.setPhase(Game.PHASE.VICTORY);
                    // zobrazeni okna s informaci o vitezstvi
                    JOptionPane.showMessageDialog(view.getFrame(),
                            "Congratulations!\n" +
                                    "Player " + player.getName() + " is the winner!" +
                                    "\n" +
                                    "New game can be started from game menu."
                    );
                    return;
                }
                // hrac dostane novou ukolovou kartu
                player.setObjective(model.getCP().popCard());
            }
        }

        // konec tahu hrace
        model.getPP().nextTurn();
        model.nextPhase();

        view.repaint();
    }

    @Override
    public void undo() {
        model.getMB().movePlayerTo(player, fromMF);

        if(PlayerFoundTreasure != null){
            // byl nalezen poklad, smazan z policka a presunuta ukolova karta do inventare

            toMF.getCard().setTreasure(PlayerFoundTreasure); // vrati se poklad na policko

            model.getCP().pushCard(player.getObjective()); // karta, ziskana po nalezeni pokladu, se vrati do balicku
            player.removeObjective(); // karta, ziskana po nalezeni pokladu, se smaze hraci

            TreasureCard playerFoundCard = player.getInventory().removeCard(player.getInventory().numFound() - 1); // vyjmuti posledni nalezene karty z inventare
            player.setObjective(playerFoundCard); // karta vyjmuta z inventare je nastavena jako hledana
        }

        if(model.getPhase().equals(Game.PHASE.VICTORY)){
            model.setPhase(Game.PHASE.MOVE);
        }
        else{
            model.getPP().prevTurn(); // predchozi tah
            model.prevPhase(); // predchozi faze
        }

        view.repaint();
    }
}
