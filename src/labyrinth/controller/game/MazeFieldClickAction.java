/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.game;


import labyrinth.controller.Commander;
import labyrinth.model.Game;
import labyrinth.model.board.MazeField;
import labyrinth.view.GameFrame;

/**
 * Reakce po kliknuti na aktivni policko.
 */
public class MazeFieldClickAction {

    /**
     * Vybere prislusnou akci.
     * @param model modelova vrstva aplikace
     * @param view zobrazovaci vrstva aplikace
     * @param mf policko hraci desky, na ktere hrac kliknul
     */
    public void execute(Game model, GameFrame view, MazeField mf) {
        Commander.Command cmd;
        switch(model.getPhase()){
            case ROTATE_AND_SHIFT:
                cmd = new ShiftCommand(model, view, mf);
                view.getInvoker().execute(cmd);
                break;
            case MOVE:
                cmd = new MoveCommand(model, view, model.getPP().getCurrentPlayer(), mf);
                view.getInvoker().execute(cmd);
                break;
            default:
                break;
        }

        view.repaint();
    }
}
