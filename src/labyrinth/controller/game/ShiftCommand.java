/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.controller.game;


import labyrinth.controller.Commander;
import labyrinth.model.Game;
import labyrinth.model.board.MazeField;
import labyrinth.view.GameFrame;

/**
 * Vlozeni volneho kamene na zadane policko.
 */
public class ShiftCommand implements Commander.Command{
    private final MazeField insertLocation;
    private final Game model;
    private final GameFrame view;

    /**
     * Vlozi volny kamen na dane policko.
     * @param model modelova vrstva aplikace
     * @param view zobrazovaci vrstva aplikace
     * @param mf policko hraci desky, na ktere hrac kliknul, policko pro vlozeni volneho kamene
     */
    public ShiftCommand(Game model, GameFrame view, MazeField mf) {
        this.model = model;
        this.view = view;
        this.insertLocation = mf;
    }

    @Override
    public void execute() {
        model.getMB().shift(insertLocation);

        model.nextPhase();

        view.repaint();
    }

    @Override
    public void undo() {
        int row = insertLocation.row();
        int col = insertLocation.col();
        int n = model.getMB().size();
        // prvni radek - kameny se posouvaly dolu ve sloupci
        if(row == 1 && col%2 == 0){
            model.getMB().shift(new MazeField(n, col));
        }
        // posledni radek - kameny se posouvaly nahoru
        else if(row == n && col%2 == 0){
            model.getMB().shift(new MazeField(1, col));
        }
        // prvni sloupec - kameny se posouvaly doprava na radku
        else if(col == 1 && row%2 == 0){
            model.getMB().shift(new MazeField(row, n));
        }
        // posledni sloupec - kameny se posouvaly doleva
        else if (col == n && row%2 == 0){
            model.getMB().shift(new MazeField(row, 1));
        }

        model.prevPhase();

        view.repaint();
    }

    public MazeField getInsertLocation() {
        return insertLocation;
    }
}
