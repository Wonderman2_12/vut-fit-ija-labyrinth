/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.model;

import java.awt.*;

/**
 * Model nastaveni nove hry.
 * Obsahuje informaci o zadanych rozmerech hraci desky, poctu pokladu, poctu hracu.
 * O jednotlivych hracich ma informaci o jejich barve a jmenu.
 */
public class NewGameSettings {
    /** rozmer hraci desky (pocet radku nebo pocet sloupcu, jedna se o ctverec)*/
    protected int MazeBoardSize;
    /** pocet pokladu */
    protected int TreasuresNum;
    /** pocet hracu */
    protected int PlayersNum;
    /** barvy hracu, indexy odpovidaji indexum v poli jmen */
    protected Color[] color;
    /** jmena hracu, indexy odpovidaji indexum v poli barev */
    protected String[] name;


    public int getMazeBoardSize() {
        return MazeBoardSize;
    }

    public void setMazeBoardSize(int mazeBoardSize) {
        MazeBoardSize = mazeBoardSize;
    }

    public int getTreasuresNum() {
        return TreasuresNum;
    }

    public void setTreasuresNum(int treasuresNum) {
        TreasuresNum = treasuresNum;
    }

    public int getPlayersNum() {
        return PlayersNum;
    }

    public void setPlayersNum(int playersNum) {
        PlayersNum = playersNum;
    }

    public Color[] getColorArray() {
        return color;
    }

    public void setColorArray(Color[] color) {
        this.color = color;
    }

    public String[] getNameArray() {
        return name;
    }

    public void setNameArray(String[] name) {
        this.name = name;
    }
}
