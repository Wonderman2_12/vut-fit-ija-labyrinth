/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 6. 4. 2015
 */


package labyrinth.model.board;


import labyrinth.model.player.Player;

/**
 * Reprezentuje policko na hraci desce.
 * Kazde policko je identifikovano pozici [row, col], ktera je nastavena pri
 * inicializaci v konstruktoru a dale se nemeni (cisluje se od 1). Na policko je
 * mozne umistit hraci kamen (viz MazeCard).
 */
public class MazeField implements java.io.Serializable{
    /** radek, na kterem je policko umisteno */
    protected final int row;
    /** sloupec, na kterem je policko umisteno */
    protected final int col;
    /** kamen, ktery je umisten na policku */
    protected MazeCard mc;


    /**
     * Vytvoreni policka na dane pozici.
     * Po vytvoreni policko neobsahuje hraci kamen.
     * @param row radek
     * @param col sloupec
     */
    public MazeField(int row, int col){
        this.row = row;
        this.col = col;
        mc = null;
    }


    /**
     * Vraci cislo radku, na kterem je policko umisteno na hraci desce.
     * @return radek pozice policka
     */
    public int row(){
        return row;
    }


    /**
     * Vraci cislo sloupce, na kterem je policko umisteno na hraci desce.
     * @return sloupec pozice policka
     */
    public int col(){
        return col;
    }


    /**
     * Vrati hraci kamen, ktery je umisten na policku.
     * Pokud na policku zadny kamen neni, vrati null.
     * @return hraci kamen nachazejici se na policku, <code>null</code> pokud na policku neni kamen
     */
    public MazeCard getCard(){
        return mc;
    }


    /**
     * Vlozi hraci kamen na policko.
     * @param c hraci kamen pro vlozeni na policko
     */
    public void putCard(MazeCard c){
        mc = c;
    }

    /**
     * Zjistuje, zda se figurka daneho hrace nachazi na kameni, ktery je na policku.
     * @param p hledany hrac
     * @return true pokud hrac je na kameni, jinak false
     */
    public boolean isThere(Player p){
        return mc.isThere(p);
    }
}

