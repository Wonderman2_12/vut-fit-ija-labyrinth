/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 6. 4. 2015
 */

 
package labyrinth.model.board;

import labyrinth.model.player.Player;
import labyrinth.model.treasure.Treasure;

import java.util.*;

/**
 * Reprezentuje jeden kamen, ktery se umistuje na policka hraci plochy.
 * Kamen uchovava informaci o ceste, tj. smerech, kudy lze kamen opustit.
 */
public class MazeCard implements java.io.Serializable{
    /** 1. smer, kudy lze opustit kamen */
    protected CANGO dir1;
    /** 2. smer, kudy lze opustit kamen */
    protected CANGO dir2;
    /** 3. smer, kudy lze opustit kamen */
    protected CANGO dir3;
    /** poklad nachazejici se na kameni */
    protected Treasure treasure;
    /** figurky nachazejici se na kameni */
    protected List<Player> players;

    
    /**
     * Vytvoreni jednoho kamene zadaneho typu.
     * @param type pismeno <code>"C"</code>, <code>"L"</code> nebo <code>"F"</code> podle typu cesty na kameni
     */
    private MazeCard(String type){
        switch (type) {
            case "C":
                dir1 = CANGO.LEFT;
                dir2 = CANGO.UP;
                dir3 = null;
                break;
            case "L":
                dir1 = CANGO.LEFT;
                dir2 = CANGO.RIGHT;
                dir3 = null;
                break;
            default: // "F"
                dir1 = CANGO.LEFT;
                dir2 = CANGO.UP;
                dir3 = CANGO.RIGHT;
                break;
        }
        treasure = null;
        players = new ArrayList<>();
    }


    /**
     * Vnoreny vyctovy typ definujici ctyri hodnoty reprezentujici smer, kudy lze
     * opustit kamen.
     */
    public enum CANGO{
        LEFT,
        UP,
        RIGHT,
        DOWN
    }

    
    /**
     * Vytvori kamen podle zadaneho typu type.
     * Typ muze byt:
     * <ul>
     * <li> "C" – vytvori kamen reprezentujici rohovou cestu (ve tvaru pismena L).
     * Po inicializaci smeruje cesta zleva nahoru.
     * <li> "L" – vytvori kamen reprezentujici rovnou cestu. Po inicializaci
     * smeruje cesta zleva doprava.
     * <li> "F" – vytvori kamen reprezentujici rozdeleni cesty (ve tvaru
     * pismena T). Po inicializaci smeruje cesta zleva nahoru a doprava.
     * </ul>
     * @param type pismeno <code>"C"</code>, <code>"L"</code> nebo <code>"F"</code> podle typu cesty na kameni
     * @return kamen zadaneho typu
     * @throws IllegalArgumentException pokud nelze kamen vytvorit, chybny retezec
     */
    public static MazeCard create(String type){
        if("C".equals(type)
        || "L".equals(type)
        || "F".equals(type)){
            return new MazeCard(type);
        }
        else{
            throw new IllegalArgumentException("Typ kamene musi byt \"C\", \"L\" nebo \"F\".");
        }
    }


    /**
     * Vraci informaci (true nebo false), zda je mozne opustit kamen danym smerem.
     * @param dir dotazovany smer opusteni kamene
     * @return true pokud je mozne opustit kamen danym smerem
     */
    public boolean canGo(MazeCard.CANGO dir){
        return (dir1 == dir
                 || dir2 == dir
                 || dir3 == dir);
    }


    /**
     * Otoci kamen o 90 stupnu doprava.
     */
    public void turnRight(){
        if(dir1 != null){dir1 = turnDirRight(dir1);}
        if(dir2 != null){dir2 = turnDirRight(dir2);}
        if(dir3 != null){dir3 = turnDirRight(dir3);}
    }

    /**
     * Nahodne otoci kamen.
     */
    public void turnRandom(){
        Random random = new Random();
        for(int i = random.nextInt(4); i > 0; i--){
            turnRight();
        }
    }
 
 
    /**
     * Otoci smer o 90 stupnu doprava.
     * @param dir smer k otoceni
     */
    private MazeCard.CANGO turnDirRight(MazeCard.CANGO dir){
        switch (dir) {
            case LEFT:
                return CANGO.UP;
            case UP:
                return CANGO.RIGHT;
            case RIGHT:
                return CANGO.DOWN;
            default: // CANGO.DOWN
                return CANGO.LEFT;
        }
    }


    /**
     * Umisti na kamen dany poklad.
     * @param treasure poklad pro umisteni na kamen
     */
    public void setTreasure(Treasure treasure) {
        this.treasure = treasure;
    }


    /**
     * Vraci poklad nachazejici se na kameni.
     * @return poklad nachazejici se na kameni, pokud kamen neobsahuje poklad, vraci null
     */
    public Treasure getTreasure(){
        return treasure;
    }


    /**
     * Odstrani poklad z kamene.
     * Odpovida pouziti setTreasure(null).
     */
    public void removeTrasure(){
        treasure = null;
    }


    /**
     * Prida na kamen figurku daneho hrace.
     * @param p hrac, jehoz figurka ma byt pridana na kamen
     */
    public void addPlayer(Player p){
        players.add(p);
    }


    /**
     * Odstrani z kamene figurku daneho hrace.
     * @param p hrac, jehoz figurka ma byt odstranena z kamene
     */
    public void removePlayer(Player p){
        players.remove(p);
    }


    /**
     * Vraci seznam hracu na kameni.
     * @return seznam hracu na kameni
     */
    public List<Player> listPlayers(){
        return players;
    }


    /**
     * Zjistuje, zda se figurka daneho hrace nachazi na kameni.
     * @param p hledany hrac
     * @return true pokud hrac je na kameni, jinak false
     */
    public boolean isThere(Player p){
        for (Player player : players) {
            if (player == p) {
                return true;
            }
        }
        return false;
    }


    /**
     * Vraci retezec popisujici stav kamene.
     * Retezec je slozen z typu kamene ("curve", "fork", "line") a cisla 0 - 3 urcujici natoceni od puvodniho stavu.
     * @return retezec popisujici typ kamene a natoceni
     */
    public String typeAndState(){
        // je treba nejdrive kontrolovat krizovatku a pak zatacku s rovinkou!
        // krizovatka
        if     (canGo(MazeCard.CANGO.LEFT)
             && canGo(MazeCard.CANGO.UP)
             && canGo(MazeCard.CANGO.RIGHT))
            return "fork0";
        else if(canGo(MazeCard.CANGO.UP)
             && canGo(MazeCard.CANGO.RIGHT)
             && canGo(MazeCard.CANGO.DOWN))
            return "fork1";
        else if(canGo(MazeCard.CANGO.RIGHT)
             && canGo(MazeCard.CANGO.DOWN)
             && canGo(MazeCard.CANGO.LEFT))
            return "fork2";
        else if(canGo(MazeCard.CANGO.DOWN)
             && canGo(MazeCard.CANGO.LEFT)
             && canGo(MazeCard.CANGO.UP))
            return "fork3";
            // rovinka
        else if(canGo(MazeCard.CANGO.LEFT)
             && canGo(MazeCard.CANGO.RIGHT))
            return "line0";
        else if(canGo(MazeCard.CANGO.UP)
             && canGo(MazeCard.CANGO.DOWN))
            return "line1";
            // zatacka
        else if(canGo(MazeCard.CANGO.LEFT)
             && canGo(MazeCard.CANGO.UP))
            return "curve0";
        else if(canGo(MazeCard.CANGO.UP)
             && canGo(MazeCard.CANGO.RIGHT))
            return "curve1";
        else if(canGo(MazeCard.CANGO.RIGHT)
             && canGo(MazeCard.CANGO.DOWN))
            return "curve2";
        else // (canGo(MazeCard.CANGO.DOWN) && canGo(MazeCard.CANGO.LEFT))
            return "curve3";
    }
}
