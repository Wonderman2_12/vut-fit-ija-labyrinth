/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 6. 4. 2015
 */


package labyrinth.model.board;

import labyrinth.model.player.Player;
import labyrinth.model.treasure.Treasure;

import java.util.*;


/**
 * Reprezentuje hraci desku.
 * Deska je rozdelena na policka (viz MazeField) a je ctvercova
 * o rozmeru n (n radku a n sloupcu). Umoznuje generovat novou hru a posunovat
 * kameny na radcich nebo sloupcich.
 */
public class MazeBoard implements java.io.Serializable{
    /** policka hraci desky, jednorozmerne pole, do ktereho se mapuje */
    protected MazeField[] fields;
    /** rozmer hraci desky (pocet radku nebo pocet sloupcu, jedna se o ctverec)*/
    protected int n;
    /** volny hraci kamen */
    protected MazeCard freeCard;


    /**
     * Vytvoreni policek hraci desky a inicializace prostoru pro volnou kartu.
     * @param n rozmer hraci plochy v poctu poicek (n radku a n sloupcu)
     */
    private MazeBoard(int n){
        fields = new MazeField[n * n];
        this.n = n;
        for(int row=0; row < n; row++){
            for(int col=0; col < n; col++){
                fields[(row) * n + col] = new MazeField(row + 1, col + 1);
            }
        }
        freeCard = null;
    }


    /**
     * Vytvori hraci plochu o velikosti n.
     * Pri inicializaci se vytvori policka (instance tridy MazeField)
     * a umisti se na spravne pozice. Kameny se nevytvareji, vznika pouze prazdna
     * hraci plocha. Pouziva konstruktor MazeBoard.
     * @param n rozmer hraci plochy v poctu policek (n radku a n sloupcu), (5, 7, 9 nebo 11)
     * @return hraci deska
     * @throws IllegalArgumentException pokud parametr n neni 5, 7, 9 ani 11
     */
    public static MazeBoard createMazeBoard(int n){
        if(n == 5
        || n == 7
        || n == 9
        || n == 11){
            return new MazeBoard(n);
        }
        else{
            throw new IllegalArgumentException("Velikost hraci plochy musi byt vetsi nez 0.");
        }
    }

    
    /**
     * Vytvori novou hru.
     * Generuje hraci kameny a umistuje je na policka. Soucasne vygeneruje jeden
     * volny kamen, ktery uchova mimo policka. Pomer mezi pocty jednotlivych typu kamenu je zhruba 1:1:1.
     * <ul>
     * <li> Rohova policka obsahuji vzdy kameny ve tvaru pismene L a to tak, ze volne cesty smeruji vzdy na hraci desku
     * (ne mimo ni); tato policka se nazyvaji startovni a figurky se na zacatku hry umistuji na tato policka
     * <li> policka, ktera jsou soucasne na lichem radku a lichem sloupci obsahuji pouze kameny ve tvaru pismene T
     * a to tak, ze volne cesty smeruji vzdy na hraci desku (ne mimo ni)
     * <li> ostatni kameny se nahodne rozmisti na volna policka; kameny se pri umistovani mohou libovolne natacet;
     * </ul>
     */
    public void newGame(){
        Random random = new Random();
        List<MazeField> freeFields= new ArrayList<>(); // policka, ktera zatim nebyla obsazena kamenem
        for(int row=1; row <= n; row++){
            for(int col=1; col <= n; col++){
                freeFields.add(get(row, col));
            }
        }

        MazeCard mcIns; // vkladany kamen
        MazeField mfIns; // policko, na ktere je vkladan kamen

        // pocitadla umistenych kamenu, "Pomer mezi pocty jednotlivych typu kamenu je zhruba 1:1:1."
        int curves = 0, forks = 0, lines = 0;

        // rohova policka - zatacky
        mcIns = MazeCard.create("C");
        // neni treba otaceni
        mfIns = get(n, n);
        mfIns.putCard(mcIns); curves++;
        freeFields.remove(mfIns); // policko od ted obsahuje kamen, neni volne

        mcIns = MazeCard.create("C");
        mcIns.turnRight();
        mfIns = get(n, 1);
        mfIns.putCard(mcIns); curves++;
        freeFields.remove(mfIns); // policko od ted obsahuje kamen, neni volne

        mcIns = MazeCard.create("C");
        mcIns.turnRight();
        mcIns.turnRight();
        mfIns = get(1, 1);
        mfIns.putCard(mcIns); curves++;
        freeFields.remove(mfIns); // policko od ted obsahuje kamen, neni volne

        mcIns = MazeCard.create("C");
        mcIns.turnRight();
        mcIns.turnRight();
        mcIns.turnRight();
        mfIns = get(1, n);
        mfIns.putCard(mcIns); curves++;
        freeFields.remove(mfIns); // policko od ted obsahuje kamen, neni volne

        // policka na lichem radku a sloupci - krizovatky
        for(int row=1; row <= n; row += 2){
            for(int col=1; col <= n; col += 2){
                // pokud zde jiz neni polozen kamen (zatacka v rohu)
                mfIns = get(row, col);
                if(mfIns.getCard() == null){
                    mcIns = MazeCard.create("F");
                    // otoceni stranou bez vychodu k okraji
                    if(row == n){
                        // neni treba otaceni
                    }
                    else if(col == 1){
                        mcIns.turnRight();
                    }
                    else if(row == 1){
                        mcIns.turnRight();
                        mcIns.turnRight();
                    }
                    else if(col == n){
                        mcIns.turnRight();
                        mcIns.turnRight();
                        mcIns.turnRight();
                    }
                    else{
                        // nejedna se o policko u okraje desky, otoceni bude nahodne
                        for(int i = random.nextInt(4); i > 0 ; i--) {
                            mcIns.turnRight();
                        }
                    }
                    // vlozeni kamene na policko
                    mfIns = get(row, col);
                    mfIns.putCard(mcIns); forks++;
                    freeFields.remove(mfIns); // policko od ted obsahuje kamen, neni volne
                }
            }
        }

        // hraci kameny na policka
        String typeMinCount;
        while( ! freeFields.isEmpty()){
            // ziskani typu kamene s nejnizsim poctem vytvorenych kamenu
            if(curves <= forks  && curves <= lines){
                typeMinCount = "C"; curves++;
            }
            else if(forks  <= curves && forks  <= lines){
                typeMinCount = "F"; forks++;
            }
            else{ // (lines  <= curves && lines  <= forks)
                typeMinCount = "L"; lines++;
            }
            mcIns = MazeCard.create(typeMinCount);
            mcIns.turnRandom(); // nahodne pootoceni
            // vyber nahodneho volneho policka
            mfIns = freeFields.remove(random.nextInt(freeFields.size()));
            mfIns.putCard(mcIns);
        }
        // volny hraci kamen
        // ziskani typu kamene s nejnizsim poctem vytvorenych kamenu
        if(curves <= forks  && curves <= lines){
            typeMinCount = "C"; curves++;
        }
        else if(forks  <= curves && forks  <= lines){
            typeMinCount = "F"; forks++;
        }
        else{ // (lines  <= curves && lines  <= forks)
            typeMinCount = "L"; lines++;
        }
        mcIns = MazeCard.create(typeMinCount);
        mcIns.turnRandom(); // nahodne pootoceni
        freeCard = mcIns;
    }


    /**
     * Nahodne rozda na policka hraci desky poklady.
     */
    public void setTreasuresRandomly(){
        Random random = new Random();
        List<MazeCard> freeCards= new ArrayList<>(); // policka, ktera zatim nebyla obsazena kamenem
        for(int row=1; row <= n; row++){
            for(int col=1; col <= n; col++){
                freeCards.add(this.get(row, col).getCard());
            }
        }

        // umisteni kazdeho z pokladu
        for(int i=0; i < Treasure.count(); i++) {
            Treasure trIns = Treasure.getTreasure(i);
            MazeCard mcIns = freeCards.remove(random.nextInt(freeCards.size())); // kamen od ted obsahuje poklad, neni volny
            mcIns.setTreasure(trIns);
        }
    }
    
    
    /**
     * Vraci rozmer hraci desky.
     * Rozmer odpovida poctu radku a sloupcu, cislovano od 1.
     * @return rozmer hraci desky
     */
    public int size(){
        return n;
    }
    
    
    /**
     * Vraci policko na pozici <code>[r, c]</code>.
     * V pripade chyby (mimo rozsah) vraci <code>null</code>. Policka jsou
     * cislovana od 1 do <code>n</code>.
     * @param r radek
     * @param c sloupec
     * @return policko na zadane pozici <code>[r, c]</code>
     */
    public MazeField get(int r, int c){
        if(0 < r && r <= n
        && 0 < c && c <= n){
            return fields[(r - 1) * n + c - 1];
        }
        else{
            return null;
        }
    }


    /**
     * Vraci volny kamen.
     * Pokud takovy kamen neni (hra nebyla vytvorena), vraci null.
     * @return volny kamen, null pokud neni volny kamen
     */
    public MazeCard getFreeCard(){
        return freeCard;
    }
    
    
    /**
     * Provede vlozeni volneho kamene na pozici policka <code>mf</code>.
     * Podle pozice policka <code>mf</code> se provede prislusne posunuti kamenu:
     * <ul>
     * <li> prvni radek <code>[1, c]</code> - kameny se posunou dolu ve sloupci
     * <code>c</code>, pokud je <code>c</code> sude
     * <li> posledni radek <code>[n, c]</code> - kameny se posunou nahoru
     * ve sloupci <code>c</code>, pokud je <code>c</code> sude
     * <li> prvni sloupec <code>[r, 1]</code> - kameny se posunou doprava na radku
     * <code>r</code>, pokud je <code>r</code> sude
     * <li> posledni sloupec <code>[r, n]</code> - kameny se posunou doleva
     * na radku <code>r</code>, pokud je <code>r</code> sude
     * </ul>
     * Na policko <code>mf</code> se vlozi volny kamen a dalsi kameny se posunou.
     * Kamen, ktery je timto postupem vysunut, se stava volnym kamenem.
     * Pokud policko <code>mf</code> nesplnuje vyse uvedene podminky, nedela
     * metoda nic.
     * Pokud se vysunutim kamene vysunou i figurky, premisti se figurky na druhou
     * stranu, tj. na nove vlozeny kamen.
     * @param mf policko, na jehoz pozici bude vlozen volny kamen
     */
    public void shift(MazeField mf){
        int col = mf.col();
        int row = mf.row();
        
        // prvni radek - kameny se posunou dolu ve sloupci
        if(row == 1 && col%2 == 0){
            int r = n;
            MazeCard outCard = this.get(r, col).getCard();
            for( ; r > 1; r--){
                this.get(r, col).putCard( this.get(r - 1, col).getCard() );
            }
            this.get(r, col).putCard(freeCard);
            freeCard = outCard;
            this.shiftPlayers(r, col); // figurky zustavaji na hraci desce
        }
        // posledni radek - kameny se posunou nahoru
        else if(row == n && col%2 == 0){
            int r = 1;
            MazeCard outCard = this.get(r, col).getCard();
            for( ; r < n; r++){
                this.get(r, col).putCard( this.get(r + 1, col).getCard() );
            }
            this.get(r, col).putCard(freeCard);
            freeCard = outCard;
            this.shiftPlayers(r, col); // figurky zustavaji na hraci desce
        }
        // prvni sloupec - kameny se posunou doprava na radku
        else if(col == 1 && row%2 == 0){
            int c = n;
            MazeCard outCard = this.get(row, c).getCard();
            for( ; c > 1; c--){
                this.get(row, c).putCard( this.get(row, c - 1).getCard() );
            }
            this.get(row, c).putCard(freeCard);
            freeCard = outCard;
            this.shiftPlayers(row, c); // figurky zustavaji na hraci desce
        }
        // posledni sloupec - kameny se posunou doleva
        else if(col == n && row%2 == 0){
            int c = 1;
            MazeCard outCard = this.get(row, c).getCard();
            for( ; c < n; c++){
                this.get(row, c).putCard( this.get(row, c + 1).getCard() );
            }
            this.get(row, c).putCard(freeCard);
            freeCard = outCard;
            this.shiftPlayers(row, c); // figurky zustavaji na hraci desce
        }
    }


    /**
     * Presune hrace, kteri se vlozenim kamene vyskytli mimo desku, na opacnou stranu desky.
     * @param rowIns radek vkladaneho kamenu
     * @param colIns sloupec vkladaneho kamenu
     */
    private void shiftPlayers(int rowIns, int colIns){
        List<Player> playersOnCard = freeCard.listPlayers();
        for (int i=0; i <  playersOnCard.size(); i++) {
            Player p = playersOnCard.get(i);
            freeCard.removePlayer(p);
            this.get(rowIns, colIns).getCard().addPlayer(p);
        }
    }


    /**
     * Zjisti, zda je mezi dvema policky po kamenech cesta.
     * Z pocatecni pozice postupuje prohledavaci metodou BFS, pri nalezeni oznami dosazitelnost vracenim hodnoty
     * <code>true</code>. Pri prohledani cele dosazitelne casti a nenalezeni pozadovaneho cile oznami nedosazitelnost
     * vracenim hodnoty <code>false</code>.
     * @param from vychozi pole pro cestu
     * @param to cilove pole pro cestu
     * @return true pokud je mozne se z <code>MazeField from</code> presunout do <code>Mazefield to</code>
     */
    public boolean isReachable(MazeField from, MazeField to){
        /* sestrojeni dvou prazdnych seznamu, fronty open pro vsechny policka k expanzi
         a closed pro jiz expandovane uzly */
        LinkedList<MazeField> open = new LinkedList<>();
        LinkedList<MazeField> closed = new LinkedList<>();
        open.add(from); // umisteni pocatecniho policka do fronty open

        // je-li frotna open prazdna, pak je prohledavani neuspesne
        while(!open.isEmpty()) {
            // vyjmuti prvniho policka z cela fronty open a umisteni do seznamu closed
            MazeField field = open.removeFirst();
            closed.add(field);
            // je-li vyjmute policko cilove, prohledavani je uspesne
            if (field.equals(to)) {
                return true;
            }
            // expanze policka
            MazeCard card = field.getCard();
            // na policku je kamen
            if (card != null) {
                // kdyz je spojeni nahoru
                // kdyz je mozne opustit nahoru
                if (card.canGo(MazeCard.CANGO.UP)) {
                    /* kdyz je nad aktualnim kamenem dalsi kamen a je mozne ho opustit dolu
                    a pokud neni tento dalsi kamen jiz ve fronte open ani v seznamu closed
                    pak se umisti do fronty open */
                    MazeField next = this.get(field.row() - 1, field.col());
                    if (next != null
                            && next.getCard().canGo(MazeCard.CANGO.DOWN)
                            && !open.contains(next)
                            && !closed.contains(next)) {
                        open.add(next);
                    }
                }

                // kdyz je spojeni doprava
                // kdyz je mozne opustit doprava
                if (card.canGo(MazeCard.CANGO.RIGHT)) {
                    /* kdyz je napravo od aktualniho kamene dalsi kamen a je mozne ho opustit doleva
                    a pokud neni tento dalsi kamen jiz ve fronte open ani v seznamu closed
                    pak se umisti do fronty open */
                    MazeField next = this.get(field.row(), field.col() + 1);
                    if (next != null
                            && next.getCard().canGo(MazeCard.CANGO.LEFT)
                            && !open.contains(next)
                            && !closed.contains(next)) {
                        open.add(next);
                    }
                }

                // kdyz je spojeni dolu
                // kdyz je mozne opustit dolu
                if (card.canGo(MazeCard.CANGO.DOWN)) {
                    /* kdyz je pod aktualnim kamenem dalsi kamen a je mozne ho opustit nahoru
                    a pokud neni tento dalsi kamen jiz ve fronte open ani v seznamu closed
                    pak se umisti do fronty open */
                    MazeField next = this.get(field.row() + 1, field.col());
                    if (next != null
                            && next.getCard().canGo(MazeCard.CANGO.UP)
                            && !open.contains(next)
                            && !closed.contains(next)) {
                        open.add(next);
                    }
                }

                // kdyz je spojeni doleva
                // kdyz je mozne opustit doleva
                if (card.canGo(MazeCard.CANGO.LEFT)) {
                    /* kdyz je nalevo od aktualniho kamene dalsi kamen a je mozne ho opustit doprava
                    a pokud neni tento dalsi kamen jiz ve fronte open ani v seznamu closed
                    pak se umisti do fronty open */
                    MazeField next = this.get(field.row(), field.col() - 1);
                    if (next != null
                            && next.getCard().canGo(MazeCard.CANGO.RIGHT)
                            && !open.contains(next)
                            && !closed.contains(next)) {
                        open.add(next);
                    }
                }
            }
        }
        return false;
    }


    /**
     * Zjistuje pozici daneho hrace na hraci desce.
     * Vraci policko hraci desky, <code>MazeField</code>
     * @param p hrac
     * @return policko hraci desky, kde se nachazi zadany hrac,
     *         null pokud hrac nebyl na desce nalezen
     */
    public MazeField whereIs(Player p){
        for(int i=0; i < n * n; i++){
            if(fields[i].isThere(p)){
                return fields[i];
            }
        }
        return null;
    }


    /**
     * Presune hrace p na policko mf.
     * @param p hrac pro presunuti
     * @param mfTo cilove policko, kam ma byt zadany hrac presunut
     */
    public void movePlayerTo(Player p, MazeField mfTo) {
        this.whereIs(p).getCard().removePlayer(p);
        mfTo.getCard().addPlayer(p);
    }
}
