/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 12. 4. 2015
 */


package labyrinth.model.player;

import labyrinth.model.treasure.TreasureCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Reprezentuje inventar hrace obsahujici nalezene poklady.
 * Kapacita inventare odpovida mnozstvi nalezenych pokladu potrebnych k vitezstvi hry.
 */
public class Inventory implements java.io.Serializable{
    /** mista pro nalezene pokladove karty */
    protected List<TreasureCard> slots;
    /** pocet nalezenych pokladu potrebnych k vitezstvi */
    protected final int goal;


    /**
     * Vytvori inventar se zadanym cilovym poctem pokladu.
     * @param goal pocet nalezenych pokladu potrebnych k vitezstvi
     */
    public Inventory(int goal){
        this.goal = goal;
        slots = new ArrayList<>();
    }


    /**
     * Vlozi zadanou kartu do inventare.
     * Pokud je inventar kompletni, nedela nic.
     * @param tc karta pro vlozeni
     */
    public void insertCard(TreasureCard tc){
        if( ! isCompleted()) {
            slots.add(tc);
        }
    }


    /**
     * Informuje, kolik nalezenych karet je v inventari.
     * @return pocet nalezenych karet
     */
    public int numFound(){
        return slots.size();
    }


    /**
     * Vraci celkovy pocet pokladu potrebnych k vitezstvi.
     * Neuvazuje pocet jiz nalezenych pokladu.
     * @return celkovy pocet pokladu potrebnych k vitezstvi
     */
    public int getGoal() {
        return goal;
    }


    /**
     * Informuje, zda je cil inventare kompletni.
     * Pokud inventar obsahuje cilovy pocet nalezenych pokladu, pak je kompletni.
     * @return true pokud je inventar kompletni
     */
    public boolean isCompleted(){
        return ! (goal > slots.size());
    }


    /**
     * Vraci kartu na zadane pozici v inventari.
     * Pokud v inventari neni na zadane pozici karta, vraci null.
     * @param i pozice v inventari
     * @return karta na zadane pozici v inventari
     */
    public TreasureCard getCard(int i) {
        if(slots.size() > i && i >= 0){
            return slots.get(i);
        }
        else{
            return null;
        }
    }

    /**
     * Vyjme a vrati kartu na zadane pozici v inventari.
     * Pokud v inventari neni na zadane pozici karta, vraci null.
     * @param i pozice v inventari
     * @return karta vyjmuta ze zadane pozice v inventari
     */
    public TreasureCard removeCard(int i) {
        if(slots.size() > i && i >= 0){
            return slots.remove(i);
        }
        else{
            return null;
        }
    }
}
