/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 12. 4. 2015
 */


package labyrinth.model.player;

import labyrinth.model.treasure.TreasureCard;

import java.awt.*;
import java.lang.*;

/**
 * Reprezentuje jednoho hrace.
 * Obsahuje i hracovu aktualni hledanou kartu objective a komponentu hracuv inventar inventory.
 */
public class Player implements java.io.Serializable{
    /** jmeno hrace */
    protected final String name;
    /** barva hrace */
    protected final Color color;
    /** hledana karta */
    protected TreasureCard objective;
    /** inventar hrace */
    protected Inventory inventory;


    /**
     * Vytvori noveho hrace se zadanym jmenem, barvou a inventarem s danym cilem.
     * Po vytvoreni nema hrac hledanou kartu a nema figurku na hraci desce.
     * @param name jmeno hrace, musi mit delku od 1 do 20 znaku vcetne
     * @param color barva hrace
     * @param goal pocet nalezenych pokladu potrebnych k vitezstvi
     * @throws IllegalArgumentException pokud jmeno hrace nema delku od 1 do 20 znaku vcetne
     */
    public Player(String name, Color color, int goal){
        if(0 < name.length() && name.length() < 21){
            this.name = name;
        }
        else{
            throw new IllegalArgumentException("Jmeno hrace musi mit delku od 1 do 20 znaku vcetne.");
        }
        this.color = color;
        objective = null;
        inventory = new Inventory(goal);
    }

    /**
     * Vraci jmeno hrace.
     * @return jmeno hrace
     */
    public String getName() {
        return name;
    }


    /**
     * Vraci barvu hrace.
     * @return barva hrace
     */
    public Color getColor() {
        return color;
    }

    /**
     * Vraci inventar hrace
     * @return inventar hrace
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Nastavi hraci hledanou kartu.
     * @param objective hledana karta
     */
    public void setObjective(TreasureCard objective) {
        this.objective = objective;
    }

    /**
     * Smaze hraci hledanou kratu.
     * Odpovida pouziti <code>setObjective(null)</code>
     */
    public void removeObjective() {
        this.objective = null;
    }


    /**
     * Vraci aktualni hledanou kartu.
     * Pokud hrac nema hledanou kartu, vrati null.
     * @return aktualni hledana karta
     */
    public TreasureCard getObjective() {
        return objective;
    }


    /**
     * Presune hledanou kartu do inventare.
     * Pouziva se pri nalezeni hledane karty.
     */
    public void moveObjectiveToInventory(){
        inventory.insertCard(objective);
        objective = null;
    }


    public String getColorName() {
        if     (color.equals(Color.RED))   { return "red"; }
        else if(color.equals(Color.GREEN)) { return "green"; }
        else if(color.equals(Color.BLUE))  { return "blue"; }
        else                               { return "yellow"; } // (playerColor.equals(Color.YELLOW))
    }
}
