/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 12. 4. 2015
 */


package labyrinth.model.player;

import java.util.ArrayList;
import java.util.List;


/**
 * Reprezentuje skupinu hracu.
 */
public class PlayerPool implements java.io.Serializable{
    /**  aktualni hraci */
    protected List<Player> players;
    /** index hrace, ktery je prave na tahu */
    protected int currentPlayerIndex;


    /**
     * Vytvori prazdnou skupinu hracu.
     */
    public PlayerPool(){
        players = new ArrayList<>();
        currentPlayerIndex = 0;
    }


    /**
     * Prida hrace do skupiny
     * @param p hrac k pridani do skupiny
     */
    public void addPlayer(Player p){
        players.add(p);
    }


    /**
     * Preda tah dalsimu hraci.
     * Pokud skupina neobsahuje zadne hrace, nedela nic.
     */
    public void nextTurn(){
        if( ! players.isEmpty()){
            currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
        }
    }


    /**
     * Vrati tah predchazejicimu hraci.
     * Pokud skupina neobsahuje zadne hrace, nedela nic.
     */
    public void prevTurn(){
        if( ! players.isEmpty()){
            currentPlayerIndex = (players.size() + currentPlayerIndex - 1) % players.size();
        }
    }


    /**
     * Vraci hrace, ktery je prave na tahu.
     * Pokud skupina neobsahuje zadne hrace, vraci null.
     * @return hrac, ktery je prave na tahu
     */
    public Player getCurrentPlayer(){
        if ( ! players.isEmpty()) {
            return players.get(currentPlayerIndex);
        }
        else{
            return null;
        }
    }


    /**
     * Vrati seznam hracu.
     * @return seznam hracu
     */
    public List<Player> listPlayers(){
        return players;
    }


    /**
     * Vrati pocet hracu.
     * @return pocet hracu
     */
    public int count(){
        return players.size();
    }
}
