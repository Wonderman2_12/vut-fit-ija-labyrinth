/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 13. 3. 2015
 */


package labyrinth.model.treasure;


/**
 * Reprezentuje jednu hrací kartu.
 * Karta zobrazuje poklad (instanci tridy {@link Treasure}).
 */
public class TreasureCard implements java.io.Serializable{

    /** poklad zobrazeny na karte */
    private Treasure tr;

    
    /**
     * Inicializace karty, nastaveni zobrazeneho pokladu tr.
     * @param tr poklad, se kterym ma byt karta vytvorena
     */
    public TreasureCard(Treasure tr){
        this.tr = tr;
    }


    public boolean equals(Object o){
        if(o instanceof TreasureCard){
            TreasureCard c = (TreasureCard) o;
            return tr.equals(c.tr);
        }
        else{
            return false;
        }
    }
    public int hashCode(){
        return tr.hashCode();
    }


    public Treasure getTr() {
        return tr;
    }


    /**
     * Textova reprezentace hraci karty je dana cislem pokladu na karte.
     * @return "TC(tr)", kde tr je cislo pokladu
     */
    @Override
    public String toString() {
        return "TC(" + tr.toString() + ")";
    }
}
