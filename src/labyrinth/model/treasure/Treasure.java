/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 13. 3. 2015
 */


package labyrinth.model.treasure;


/**
 * Reprezentuje jeden poklad.
 * Kazdy poklad je identifikovan svym kodem. Kody se prideluji vzestupne od
 * hodnoty 0. Kazdy objekt tridy <code>Treasure</code> je nemenny. Trida
 * obsahuje na staticke urovni pole uchovavajici vygenerovany set pokladu
 * (viz createSet), dale se pouzivaji objekty z tohoto setu
 * (viz getTreasure).
 */
public class Treasure implements java.io.Serializable{
    /** kod pokladu "Kazdy objekt tridy Treasure je nemenny." */
    private final int code;
    /** vygenerovany set pokladu */
    private static Treasure [] trSet;
    /** pocet vygenerovanych pokladu */
    private static int treasureCount;

    
    /**
     * Inicializace objektu, nastaveni jeho kodu <code>code</code>. Volano pouze
     * z metody createSet.
     * @param code kod pro identifikaci nove vytvoreneho pokladu
     */
    private Treasure(int code){
        this.code=code;
    }


    public boolean equals(Object o){
        if(o instanceof Treasure){
            Treasure t = (Treasure) o;
            return (code == t.code);
        }
        else{
            return false;
        }
    }
    public int hashCode(){
        return code;
    }

    
    /**
     * Vytvori set pokladu, ktere lze pote pouzivat.
     * Pocet vytvorenych objektu muze byt 12 nebo 24.
     * @param n pocet pokladu v nove vytvorenem setu, 12 nebo 24
     * @throws IllegalArgumentException pokud parametr n neni 12 ani 24
     */
    public static void createSet(int n){
        if(n == 12 || n == 24){
            treasureCount = n;
            trSet = new Treasure[n];
            for(int i=0; i < trSet.length; i++){
                trSet[i] = new Treasure(i);
            }
        }
        else{
            throw new IllegalArgumentException("Pocet vytvorenych objektu muze byt 12 nebo 24.");
        }
    }


    public static int count() {
        return treasureCount;
    }


    public int getCode() {
        return code;
    }

    
    /**
     * Z vytvoreneho setu vraci objekt pokladu odpovidajici zadanemu kodu
     * <code>code</code>.
     * Pokud takovy objekt neexistuje, vraci null.
     * @param code kod pokladu pro ziskani
     * @return poklad ze setu s zadanym kodem
     */
    public static Treasure getTreasure(int code){
        if(0 <= code && code < trSet.length){ // kod existuje
            return trSet[code];
        }
        else{ // kod neexistuje
            return null;
        }
    }

    /**
     * Textova reprezentace pokladu je dana jeho cislem.
     * @return text cisla pokladu
     */
    @Override
    public String toString() {
        return String.valueOf(code);
    }
}
