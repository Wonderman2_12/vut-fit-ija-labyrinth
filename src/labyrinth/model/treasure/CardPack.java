/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 13. 3. 2015
 */


package labyrinth.model.treasure;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Reprezentuje balicek hracich karet (instance tridy {@link TreasureCard}).
 */
public class CardPack implements java.io.Serializable{
    /** balicek karet */
    protected List<TreasureCard> pack;

    
    /**
     * Inicializace balicku parametry <code>maxSize</code> (maximalni pocet karet
     * v balicku) a <code>initSize</code> (pocatecni pocet karet v balicku).
     * Musi platit, ze <code>maxSize</code> &ge; <code>initSize</code>. Naplni
     * balicek kartami, ktere vytvori. Karty jsou v balicku usporadany vzestupne
     * podle kodu pokladu (vrchni karta reprezentuje poklad s kodem 0, dalsi karta 
     * poklad s kodem 1 atd.)
     * @param maxSize maximalni pocet karet v balicku
     * @param initSize pocatecni pocet karet v balicku
     * @throws IllegalArgumentException pokud neplati, ze maxSize &ge; initSize
     */
    public CardPack(int maxSize, int initSize){
        if(maxSize >= initSize){ // korektni parametry
            pack = new ArrayList<>(maxSize);
            for(int i=0; i < initSize; i++){ // napleneni balicku kartami
                pack.add(0, new TreasureCard(Treasure.getTreasure(i)));
            }
        }
        else{
            throw new IllegalArgumentException("Musi platit, ze maxSize >= initSize.");
        }
    }


    /**
     * Vyjme vrchni kartu z balicku a vrati ji (pocet karet v balicku se snizi).
     * @return karta vyjmuta z vrcholu balicku
     */
    public TreasureCard popCard(){
        return pack.remove(0);
    }


    /**
     * Zadanou kartu vlozi na vrchol balicku (pocet karet v balicku se zvysi).
     * @param tc karta pro vlozeni do balicku
     */
    public void pushCard(TreasureCard tc){
        pack.add(0, tc);
    }


    /**
     * Vraci aktualni pocet karet v balicku.
     * @return pocet karet v balicku
     */
    public int size(){
        return pack.size();
    }


    /**
     * Zamicha kartami.
     * Po teto operaci se zmeni usporadani karet v balicku. Pouziva tridu
     * <code>java.util.Random</code>. Implementovano jako Fisher-Yates shuffle.
     */
    public void shuffle(){
        Random random = new Random();
        int size = pack.size();
        for(int i=size-1; i > 0; i--){
                int swapIndex = random.nextInt(i+1);
                TreasureCard swapCard = pack.get(i);
                pack.set(i, pack.get(swapIndex));
                pack.set(swapIndex, swapCard);
        }
    }
}
