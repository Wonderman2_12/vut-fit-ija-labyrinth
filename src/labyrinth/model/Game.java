/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.model;

import labyrinth.model.board.MazeBoard;
import labyrinth.model.player.PlayerPool;
import labyrinth.model.treasure.CardPack;

/**
 * Modelova vrstva aplikace.
 * Obsahuje modely jednotlivych casti.
 */
public class Game implements java.io.Serializable{
    /** hraci deska */
    protected MazeBoard mb;
    /** balicek ukolovych karet pokladu */
    protected CardPack cp;
    /** hraci */
    protected PlayerPool pp;

    public String getImageSizeVariant() {
        return imageSizeVariant;
    }

    protected String imageSizeVariant;

    public int getMazeCardSize(){
        return mazeElementSize;
    }
    public int getMazeFieldSize(){
        return mazeElementSize;
    }

    protected int mazeElementSize;

    /** faze hry */
    protected PHASE phase;


    /**
     * Vytvori model ze zadanych casti.
     * @param mazeBoard hraci deska
     * @param cardPack balicek ukolovych karet
     * @param playerPool skupina hracu
     */
    public Game(MazeBoard mazeBoard, CardPack cardPack, PlayerPool playerPool){
        mb = mazeBoard;
        cp = cardPack;
        pp = playerPool;

        imageSizeVariant = mb.size() + "x" + mb.size();
        switch (mb.size()){
            case 5:
                mazeElementSize = 100;
                break;
            case 7:
                mazeElementSize = 80;
                break;
            case 9:
                mazeElementSize = 60;
                break;
            default: // mazeBoardSize == 11
                mazeElementSize = 50;
                break;
        }

        phase = PHASE.ROTATE_AND_SHIFT;

    }

    /**
     * Vnoreny vyctovy typ definujici faze hry.
     */
    public enum PHASE{
        ROTATE_AND_SHIFT,
        MOVE,
        VICTORY,;
    }

    public String getPhaseString() {
        switch(phase){
            case ROTATE_AND_SHIFT:
                return "rotate and shift";
            case MOVE:
                return "move";
            case VICTORY:
                return "victory";
        }
        return "";
    }

    public PHASE getPhase() {
        return phase;
    }

    public void setPhase(PHASE phase) {
        this.phase = phase;
    }

    public void nextPhase() {
        switch(phase){
            case ROTATE_AND_SHIFT:
                phase = PHASE.MOVE;
                break;
            case MOVE:
                phase = PHASE.ROTATE_AND_SHIFT;
                break;
        }
    }

    public void prevPhase() {
        switch(phase){
            case MOVE:
                phase = PHASE.ROTATE_AND_SHIFT;
                break;
            case ROTATE_AND_SHIFT:
                phase = PHASE.MOVE;
                break;
        }
    }

    public MazeBoard getMB() {
        return mb;
    }

    public CardPack getCP() {
        return cp;
    }

    public PlayerPool getPP() {
        return pp;
    }
}
