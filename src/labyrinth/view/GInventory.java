/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.model.Game;
import labyrinth.model.treasure.TreasureCard;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Graficka reprezentace inventare zadaneho hrace.
 */
public class GInventory extends JPanel {
    private Game model;

    private List<JLabel> lInventorySlots = new ArrayList<>();

    /**
     * Vytvori grafickou reprezentaci inventare aktualniho hrace.
     * @param model skupina uzivatelu
     */
    public GInventory(Game model) {
        this.model = model;

        GridBagLayout gbl = new GridBagLayout();
        gbl.columnWidths = new int[] {100};
        gbl.rowHeights = new int[] {40, 600};
        gbl.columnWeights = new double[]{1.0};
        gbl.rowWeights = new double[]{0.0, 1.0};
        this.setLayout(gbl);

        // text label Inventory
        JLabel lblInventory = new JLabel("Inventory");
        GridBagConstraints gbc_lblInventory = new GridBagConstraints();
        gbc_lblInventory.insets = new Insets(0, 0, 5, 0);
        gbc_lblInventory.gridx = 0;
        gbc_lblInventory.gridy = 0;
        this.add(lblInventory, gbc_lblInventory);

        // panel, do ktereho se ukladaji ziskane karty - je to GridLayout se dvema sloupci a radami dle potreby
        JPanel pInventorySlots = new JPanel();
        GridBagConstraints gbc_panel_2 = new GridBagConstraints();
        gbc_panel_2.fill = GridBagConstraints.BOTH;
        gbc_panel_2.gridx = 0;
        gbc_panel_2.gridy = 1;
        this.add(pInventorySlots, gbc_panel_2);
        pInventorySlots.setLayout(new GridLayout(0, 2, 10, 10));

        // ziskame velikost inventare a podle toho vytvorime spravny pocet JLabelu
        int InvSize = model.getPP().getCurrentPlayer().getInventory().getGoal();
        for(int i = 0; i < InvSize; i++){
            JLabel slot = new JLabel("");
            pInventorySlots.add(slot);
            lInventorySlots.add(slot);

            TreasureCard invTC = model.getPP().getCurrentPlayer().getInventory().getCard(i); // karta na danem miste inventare
            if(invTC != null){
                int currTCcode = invTC.getTr().getCode();
                Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/" + model.getImageSizeVariant() + "/" + currTCcode + ".png")).getImage();
                slot.setIcon(new ImageIcon(img));
            }
            else{
                Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/empty.png")).getImage();
                slot.setIcon(new ImageIcon(img));
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

       for(int i=0; i < lInventorySlots.size(); i++){
           TreasureCard invTC = model.getPP().getCurrentPlayer().getInventory().getCard(i); // karta na danem miste inventare
           if(invTC != null){
               int currTCcode = invTC.getTr().getCode();
               Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/" + model.getImageSizeVariant() + "/" + currTCcode + ".png")).getImage();
               lInventorySlots.get(i).setIcon(new ImageIcon(img));
           }
           else{
               Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/empty.png")).getImage();
               lInventorySlots.get(i).setIcon(new ImageIcon(img));
           }
       }
    }


}
