/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.controller.app.CreateNewGameAction;
import labyrinth.model.NewGameSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Reprezentuje okno GUI nabidky zadani jmen hracu.
 * Toto okno se zobrazi pri klinuti na tlacitko Next okne nabidky nove hry.
 */
public class NewPlayersFrame extends JFrame {
    private JFrame frame;


    /**
     * Vytvori okno GUI nabidky zadani jmen hracu.
     * Validni jmeno je 1 — 20 znaku dlouhe.
     * Pokud hrac zada vice znaku, jmeno se orizne na 20, pokud nezada nic, nastavi se PlayerX, kde X je poradove cislo hrace.
     * @param settings nastaveni nove hry
     */
    public NewPlayersFrame(NewGameSettings settings) {
        frame = new JFrame("Labyrinth: Players");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 305, 121);
        //frame.getContentPane().setLayout(null);
        frame.toFront(); // do popredi

        frame.getContentPane().setLayout(new BorderLayout(0, 0));

        JLabel lblChooseYourNames = new JLabel("Choose your names:");
        lblChooseYourNames.setHorizontalAlignment(SwingConstants.CENTER);
        frame.getContentPane().add(lblChooseYourNames, BorderLayout.NORTH);

        JButton btnNext = new JButton("Next");
        frame.getContentPane().add(btnNext, BorderLayout.SOUTH);

        // na kazde strane BorderLayoutu jedno "roztahovatko" pro hezci rozlozeni
        Component horizontalStrut = Box.createHorizontalStrut(20);
        frame.getContentPane().add(horizontalStrut, BorderLayout.WEST);
        Component horizontalStrut_1 = Box.createHorizontalStrut(20);
        frame.getContentPane().add(horizontalStrut_1, BorderLayout.EAST);

        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridLayout(0, 2, 0, 0));

            // zde se vytvori spravny pocet dvojic JLabel + JTextField - pro kazdeho hrace
            int playersNum = settings.getPlayersNum();
            JTextField[] nameFields = new JTextField[playersNum];
            String[] nameLabels = new String[4];

            nameLabels[0] = "Red player:";
            nameLabels[1] = "Green player:";
            nameLabels[2] = "Blue player:";
            nameLabels[3] = "Yellow player:";

            // na kazdeho hrace vytvorime textove pole pro zadani jmena a pred nim popisek
            // nutno zachovavat poradi vkladani kvuli GridLayoutu
            for(int i = 0; i < playersNum; i++){
                panel.add(new JLabel(nameLabels[i]));
                JTextField txtName = new JTextField();
                panel.add(txtName);
                txtName.setColumns(20);
                nameFields[i] = txtName;
            }

            if(playersNum >= 3) {
                // pokud je hracu vic nez dva, potrebujeme vetsi okno
                frame.setBounds(100, 100, 305, 161);
            }

        // po kliknuti na Next se zapsane hodnoty zaslou do settings
        btnNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int playersNum = settings.getPlayersNum();
                String[] playersNames = new String[playersNum];
                Color[] playersColors = new Color[playersNum];

                // kazdemu hraci priradime hodnotu, kterou zadal do textoveho pole
                for(int i = 0; i < playersNum; i++){
                    String check = nameFields[i].getText();
                    // znaku musi byt 1 — 20
                    if(check.length() < 1){
                        check = "Player" + Integer.toString(i+1);
                    }
                    else if(check.length() > 20){
                        check = check.substring(0, 20);
                    }
                    playersNames[i] = check;
                }

                // kazdemu hraci priradime barvu
                playersColors[0] = Color.RED;
                playersColors[1] = Color.GREEN;
                if(playersNum >= 3){
                    playersColors[2] = Color.BLUE;
                    if(playersNum == 4){
                        playersColors[3] = Color.YELLOW;
                    }
                }

                settings.setColorArray(playersColors);
                settings.setNameArray(playersNames);

                new CreateNewGameAction().execute(frame, settings);
            }
        });


        frame.setResizable(false);

        // zobrazeni okna uprostred obrazovky
        Toolkit t = Toolkit.getDefaultToolkit();
        frame.setLocation(t.getScreenSize().width / 2 - frame.getWidth() / 2,
                t.getScreenSize().height / 2 - frame.getHeight() / 2);

        frame.setVisible(true);
    }
}
