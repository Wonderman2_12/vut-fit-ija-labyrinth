/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.controller.Commander;
import labyrinth.controller.app.*;
import labyrinth.model.Game;
import labyrinth.model.treasure.TreasureCard;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Reprezentuje okno GUI hry s labyrintem.
 */
public class GameFrame{
    /** Modelova vrstva aplikace */
    protected Game model;
    /** okno GUI */
    private JFrame frame;

    public JFrame getFrame() {
        return frame;
    }

    public Commander.Invoker getInvoker() {
        return invoker;
    }

    private final JMenuBar menuBar;
    private GLeftPanel gLP;
    private GMazeBoard gMB;
    private GInventory gInv;
    private GPlayerPool gPP;

    private Commander.Invoker invoker = new Commander.Invoker();


    /**
     * Vytvori okno GUI hry s labyrintem.
     * @param model modelova vrstva aplikace
     */
    public GameFrame(Game model){
        this.model = model;
        frame = new JFrame("Labyrinth");
        frame.getContentPane().setLayout(new BorderLayout(5, 5)); // urceni layoutu celeho okna - Bordelayout
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try{
            frame.setIconImage(ImageIO.read(this.getClass().getResource("/img/icon.png")));
        }
        catch (Exception ex) {
            System.out.println("Error occured during loading resource.");
            JOptionPane.showMessageDialog(frame, "Error occured during loading resource.");
            System.exit(-1);
        }

        int size = model.getMB().size();
        if(size < 9){
            frame.setBounds(100, 100, 800, 600);
        }
        else{
            frame.setBounds(100, 100, 790, 645);
        }

        // vytvoreni listy hlavni nabidky
        menuBar = new GameFrame.MenuBar();
        frame.setJMenuBar(menuBar);

        // vytvoreni leveho panelu
        gLP = new GLeftPanel(model, this);
        frame.getContentPane().add(gLP, BorderLayout.WEST);

        // vytvoreni praveho panelu - inventar
        gInv = new GInventory(model);
        frame.getContentPane().add(gInv, BorderLayout.EAST);

        // vytvoreni hraci desky
        gMB = new GMazeBoard(model, this);
        frame.getContentPane().add(gMB, BorderLayout.CENTER);

        // vytvoreni zapati
        gPP = new GPlayerPool(model, this);
        frame.getContentPane().add(gPP, BorderLayout.SOUTH);

        frame.setResizable(false);

        // zacina se fazi ROTATE_AND_SHIFT
        this.setGUIByPhase(model.getPhase());

        // zobrazeni okna uprostred obrazovky
        Toolkit t = Toolkit.getDefaultToolkit();
        frame.setLocation(t.getScreenSize().width / 2 - frame.getWidth() / 2,
                t.getScreenSize().height / 2 - frame.getHeight() / 2);

        frame.setVisible(true);
    }

    public void setGUIByPhase(Game.PHASE phase) {
        switch(phase){
            case ROTATE_AND_SHIFT:
                gLP.btnRotate.setEnabled(true); // je mozne rotovat
                gLP.btnUndo.setEnabled(true); // je mozne undo
                gMB.setActiveShiftableFields(); // je mozne klikat na vkladatelna policka
                break;
            case MOVE:
                gLP.btnRotate.setEnabled(false); // neni mozne rotovat
                gLP.btnUndo.setEnabled(true); // je mozne undo
                gMB.setActiveReachableFields(); // je mozne klikat pouze na dosazitelna policka
                break;
            case VICTORY:
                gLP.btnRotate.setEnabled(false); // neni mozne rotovat
                // Po vitezstvi je mozne vratit tah             gLP.btnUndo.setEnabled(false); // neni mozne undo
                gMB.setEnabledAllFields(false); // neni mozne klikat na policka
        }
    }


    public void repaint() {
        this.setGUIByPhase(model.getPhase());

        menuBar.repaint();
        gLP.repaint();
        gMB.repaint();
        gInv.repaint();
        gPP.repaint();
    }


    /**
     * Reprezentuje nabidku herniho okna.
     */
    public class MenuBar extends JMenuBar{
        public MenuBar() {
            // Game menu
            JMenu mGame = new JMenu("Game");
            this.add(mGame);

            JMenuItem miNewGame = new JMenuItem("New game");
            miNewGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
            miNewGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new NewGameAction().execute(frame);
                }
            });
            mGame.add(miNewGame);

            JMenuItem miSaveGame = new JMenuItem("Save game");
            miSaveGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
            miSaveGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new SaveGameAction().execute(model, frame);
                }
            });
            mGame.add(miSaveGame);

            JMenuItem miLoadGame = new JMenuItem("Load game");
            miLoadGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
            miLoadGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new LoadGameAction().execute(frame);
                }
            });
            mGame.add(miLoadGame);

            JMenuItem miQuit = new JMenuItem("Quit");
            miQuit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new QuitAction().execute(frame);
                }
            });
            mGame.add(miQuit);


            // Help menu
            JMenu mHelp = new JMenu("Help");
            this.add(mHelp);

            JMenuItem miShowHelp = new JMenuItem("Show help");
            miShowHelp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new ShowHelpAction().execute(frame);
                }
            });
            mHelp.add(miShowHelp);

            JMenuItem miAbout = new JMenuItem("About");
            miAbout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new ShowAboutAction().execute(frame);
                }
            });
            mHelp.add(miAbout);
        }
    }

    /**
     * Reprezentuje levy panel hraciho planu.
     * Levy panel obsahuje:
     * <ul>
     * <li>jmeno hrajiciho hrace
     * <li>jeho aktualni ukolovou kartu
     * <li>volny kamen
     * <li>tlacitko Rotate
     * <li>tlacitko Undo
     * </ul>
     */
    public class GLeftPanel extends JPanel{
        Game model;
        GameFrame view;

        private JLabel lCurrentPlayer;
        private JLabel lPhase;
        private JLabel lCurrentTreasureCard;
        private GFreeMazeCard gFreeMazeCard;
        private JButton btnRotate;
        private JButton btnUndo;

        public GLeftPanel(Game model, GameFrame view) {
            this.model = model;
            this.view = view;

            int leftPanelWidth;
            switch (model.getMB().size()){
                case 5:
                    leftPanelWidth = 150;
                    break;
                case 7:
                    leftPanelWidth = 130;
                    break;
                case 9:
                    leftPanelWidth = 110;
                    break;
                default: // mazeBoardSize == 11
                    leftPanelWidth = 100;
                    break;
            }
            GridBagLayout gbl = new GridBagLayout();
            gbl.columnWidths = new int[]{leftPanelWidth};
            gbl.rowHeights = new int[]{40, 40, 100, 100, 30, 150}; // hrac, faze, karta, kamen, rotate, undo
            gbl.columnWeights = new double[]{1.0};
            gbl.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0};
            this.setLayout(gbl);

            // jmeno hrace na tahu
            lCurrentPlayer = new JLabel(model.getPP().getCurrentPlayer().getName());
            lCurrentPlayer.setForeground(model.getPP().getCurrentPlayer().getColor());
            GridBagConstraints gbc_lblCurrentPlayer = new GridBagConstraints();
            gbc_lblCurrentPlayer.insets = new Insets(0, 0, 5, 0);
            gbc_lblCurrentPlayer.gridx = 0;
            gbc_lblCurrentPlayer.gridy = 0;
            this.add(lCurrentPlayer, gbc_lblCurrentPlayer);

            // faze hry
            lPhase = new JLabel(model.getPhaseString());
            GridBagConstraints gbc_lPhase = new GridBagConstraints();
            gbc_lPhase.insets = new Insets(0, 0, 5, 0);
            gbc_lPhase.gridx = 0;
            gbc_lPhase.gridy = 1;
            this.add(lPhase, gbc_lPhase);

            // aktualni ukolova karta hrajiciho hrace
            lCurrentTreasureCard = new JLabel("");
            GridBagConstraints gbc_lCurrentTreasurecard = new GridBagConstraints();
            gbc_lCurrentTreasurecard.insets = new Insets(0, 0, 5, 0);
            gbc_lCurrentTreasurecard.gridx = 0;
            gbc_lCurrentTreasurecard.gridy = 2;
            TreasureCard currTC = model.getPP().getCurrentPlayer().getObjective(); // aktualni hledana karta
            if(currTC != null){
                int currTCcode = currTC.getTr().getCode();
                Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/" + model.getImageSizeVariant() + "/" + currTCcode + ".png")).getImage();
                lCurrentTreasureCard.setIcon(new ImageIcon(img));
            }
            else{
                Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/empty.png")).getImage();
                lCurrentTreasureCard.setIcon(new ImageIcon(img));
            }
            this.add(lCurrentTreasureCard, gbc_lCurrentTreasurecard);

            // vytvoreni volneho kamene
            GridBagConstraints gbcFreeMazeCard = new GridBagConstraints();
            gbcFreeMazeCard.insets = new Insets(0, 0, 5, 0);
            gbcFreeMazeCard.fill = GridBagConstraints.BOTH;
            gbcFreeMazeCard.gridx = 0;
            gbcFreeMazeCard.gridy = 3;
            gFreeMazeCard = new GFreeMazeCard(model);
            this.add(gFreeMazeCard, gbcFreeMazeCard);

            // button na otaceni volneho kamene
            btnRotate = new JButton("Rotate");
            GridBagConstraints gbc_btnRot = new GridBagConstraints();
            gbc_btnRot.insets = new Insets(0, 0, 5, 0);
            gbc_btnRot.gridx = 0;
            gbc_btnRot.gridy = 4;
            btnRotate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Commander.Command cmd = new RotateFreeCardCommand();
                    invoker.execute(cmd);
                }
            });
            this.add(btnRotate, gbc_btnRot);

            // button Undo
            btnUndo = new JButton("Undo");
            GridBagConstraints gbc_btnUndo = new GridBagConstraints();
            gbc_btnUndo.insets = new Insets(0, 0, 5, 0);
            gbc_btnUndo.gridx = 0;
            gbc_btnUndo.gridy = 5;
            btnUndo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    invoker.undo();
                }
            });
            this.add(btnUndo, gbc_btnUndo);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            lCurrentPlayer.setText(model.getPP().getCurrentPlayer().getName());
            lCurrentPlayer.setForeground(model.getPP().getCurrentPlayer().getColor());

            lPhase.setText(model.getPhaseString());

            TreasureCard currTC = model.getPP().getCurrentPlayer().getObjective(); // aktualni hledana karta
            if(currTC != null){
                int currTCcode = currTC.getTr().getCode();
                Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/" + model.getImageSizeVariant() + "/" + currTCcode + ".png")).getImage();
                lCurrentTreasureCard.setIcon(new ImageIcon(img));
            }
            else{
                Image img = new ImageIcon(this.getClass().getResource("/img/treasure/TreasureCard/empty.png")).getImage();
                lCurrentTreasureCard.setIcon(new ImageIcon(img));
            }

            gFreeMazeCard.repaint();

            btnRotate.repaint();

            btnUndo.repaint();
        }


        /**
         * Otoceni volnym kamenem doprava.
         */
        public class RotateFreeCardCommand implements Commander.Command{
            @Override
            public void execute() {
                model.getMB().getFreeCard().turnRight();

                repaint();
            }

            @Override
            public void undo() {
                model.getMB().getFreeCard().turnRight();
                model.getMB().getFreeCard().turnRight();
                model.getMB().getFreeCard().turnRight();

                repaint();
            }
        }
    }
}
