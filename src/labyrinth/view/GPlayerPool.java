/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.model.Game;
import labyrinth.model.player.Player;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Grafická reprezentace skupiny hracu.
 */
public class GPlayerPool extends JPanel {
    Game model;
    GameFrame view;

    private List<JLabel> lPlayers = new ArrayList<>();

    public GPlayerPool(Game model, GameFrame view) {
        this.model = model;
        this.view = view;

        this.setLayout(new GridLayout(1, 0, 0, 0));
        this.setOpaque(true);

        for(Player p : model.getPP().listPlayers()){
            JLabel gP = new JLabel(p.getName() +
                    p.getInventory().numFound() +
                    "/" +
                    p.getInventory().getGoal()
            );
            gP.setForeground(p.getColor());
            lPlayers.add(gP);
            this.add(gP);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for(int i = 0; i < lPlayers.size(); i++){
            Player p = model.getPP().listPlayers().get(i);
            JLabel l = lPlayers.get(i);
            l.setText(p.getName() +
                    ": " +
                    p.getInventory().numFound() +
                    "/" +
                    p.getInventory().getGoal() +
                    " " +
                    (p.getInventory().isCompleted() ? "<- Winner" : "")
            );
            l.setForeground(p.getColor());
        }
    }
}
