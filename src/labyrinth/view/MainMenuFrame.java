/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.controller.app.LoadGameAction;
import labyrinth.controller.app.NewGameAction;
import labyrinth.controller.app.QuitAction;
import labyrinth.controller.app.ShowAboutAction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Reprezentuje okno GUI hlavni nabidky.
 */
public class MainMenuFrame extends JFrame {
    private JFrame frame;


    /**
     * Vytvori okno GUI hlavni nabidky.
     */
    public MainMenuFrame() {
        frame = new JFrame("Labyrinth");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 240, 360);
        frame.getContentPane().setLayout(null);
        frame.toFront(); // do popredi

        // pismo hlavni nabidky
        Font font = new Font("Tahoma", Font.PLAIN, 20);

        // vytvoreni tlacitek
        JLabel lblLabyrinth = new JLabel("Labyrinth");
        lblLabyrinth.setHorizontalAlignment(SwingConstants.CENTER);
        lblLabyrinth.setBounds(10, 5, 204, 51);
        lblLabyrinth.setFont(font);
        frame.getContentPane().add(lblLabyrinth);

        JButton btnNewGame = new JButton("New game");
        btnNewGame.setBounds(33, 70, 155, 43);
        btnNewGame.setFont(font);
        btnNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NewGameAction().execute(frame);
            }
        });
        frame.getContentPane().add(btnNewGame);

        JButton btnLoadGame = new JButton("Load game");
        btnLoadGame.setBounds(33, 124, 155, 43);
        btnLoadGame.setFont(font);
        btnLoadGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { new LoadGameAction().execute(frame); }
        });
        frame.getContentPane().add(btnLoadGame);

        JButton btnAbout = new JButton("About");
        btnAbout.setBounds(33, 178, 155, 43);
        btnAbout.setFont(font);
        btnAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ShowAboutAction().execute(frame);
            }
        });
        frame.getContentPane().add(btnAbout);

        JButton btnQuit = new JButton("Quit");
        btnQuit.setBounds(33, 232, 155, 43);
        btnQuit.setFont(font);
        btnQuit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new QuitAction().execute(frame);
            }
        });
        frame.getContentPane().add(btnQuit);

        frame.setResizable(false);

        // zobrazeni okna uprostred obrazovky
        Toolkit t = Toolkit.getDefaultToolkit();
        frame.setLocation(t.getScreenSize().width / 2 - frame.getWidth() / 2,
                t.getScreenSize().height / 2 - frame.getHeight() / 2);

        frame.setVisible(true);
    }
}
