/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.controller.game.MazeFieldClickAction;
import labyrinth.model.Game;
import labyrinth.model.board.MazeField;
import labyrinth.model.player.Player;
import labyrinth.model.treasure.Treasure;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Graficka reprezentace policka herni desky.
 */
public class GMazeField extends JLayeredPane {
    protected Game model;
    protected GameFrame view;
    protected MazeField mazeFieldModel;
    protected int row;
    protected int col;

    protected JButton button;
    protected java.util.List<JLabel> lPlayers = new ArrayList<>();
    protected JLabel lPlayer;
    protected JLabel lblCurrentstoneTreasure;
    protected JLabel lblCurrentstone;

    public GMazeField(Game model, GameFrame view, int row, int col){
        this.model = model;
        this.view = view;
        this.row = row;
        this.col = col;
        this.mazeFieldModel = model.getMB().get(row, col);

        int mazeFieldSize = model.getMazeFieldSize();

        // volny hraci kamen - pouze chodba bez pripadneho pokladu
        lblCurrentstone = new JLabel();
        lblCurrentstone.setBounds(0, 0, mazeFieldSize, mazeFieldSize);
        lblCurrentstone.setIcon(
                new ImageIcon(
                        new ImageIcon(
                                this.getClass().getResource("/img/board/MazeCard/" + model.getImageSizeVariant() + "/" +
                                        mazeFieldModel.getCard().typeAndState() + ".png")
                        ).getImage()
                )
        );
        lblCurrentstone.setOpaque(true);
        this.add(lblCurrentstone, new Integer(0));

        // na hracim kameni se zobrazi poklad
        lblCurrentstoneTreasure = new JLabel("");
        lblCurrentstoneTreasure.setBounds(0, 0, mazeFieldSize, mazeFieldSize);
        lblCurrentstoneTreasure.setOpaque(false);
        Treasure mazeCardTreasure = mazeFieldModel.getCard().getTreasure();
        if(mazeCardTreasure != null){
            lblCurrentstoneTreasure.setIcon(
                    new ImageIcon(
                            new ImageIcon(
                                    this.getClass().getResource("/img/treasure/Treasure/" + model.getImageSizeVariant() + "/" + mazeCardTreasure.getCode() + ".png")
                            ).getImage()
                    )
            );
        }
        else{
            /* pokud na kameni neni poklad, je treba zobrazit ZADNY poklad
             * Kdyz se posouva poklad s kameny, zpusobovalo to, ze se ikona pokladu nechala na kameni i kdyz byl poklad
             * null. Je treba rici, ze predchozi ikona zde jiz nema byt.
             */
            lblCurrentstoneTreasure.setIcon(null);
        }
        this.add(lblCurrentstoneTreasure, new Integer(1));


        int playersNum = mazeFieldModel.getCard().listPlayers().size();
        int step = mazeFieldSize / (playersNum + 1);
        // na hracim kameni se zobrazi hraci
        for(int i=0; i < playersNum; i++){
            Player p = mazeFieldModel.getCard().listPlayers().get(i);
            lPlayer = new JLabel();
            lPlayer.setBounds(-(mazeFieldSize / 2) + (i + 1) * step, 0, mazeFieldSize, mazeFieldSize);
            lPlayer.setOpaque(false);
            String colorName = p.getColorName();

            lPlayer.setIcon(
                    new ImageIcon(
                            new ImageIcon(
                                    this.getClass().getResource("/img/player/Player/" + model.getImageSizeVariant() + "/figure_" + colorName + ".png")
                            ).getImage()
                    )
            );
            this.add(lPlayer, new Integer(2));
            lPlayers.add(lPlayer);
        }

        this.setOpaque(false);
        this.setPreferredSize(new Dimension(mazeFieldSize, mazeFieldSize));
        this.button = new JButton("");
        button.setBounds(0, 0, mazeFieldSize, mazeFieldSize);
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MazeFieldClickAction().execute(model, view, mazeFieldModel);
            }
        });
        this.add(button, new Integer(10));

        setEnabled(true);
    }

    /**
     * Urcuje, zda je mozne kliknout na policko.
     * @param f pokud je true, je na policko mozne kliknout, pokud je false, pak na policko neni mozne kliknout
     */
    public void setEnabled(boolean f){
        button.setEnabled(f);
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        lblCurrentstone.setIcon(
                new ImageIcon(
                        new ImageIcon(
                                this.getClass().getResource("/img/board/MazeCard/" + model.getImageSizeVariant() + "/" +
                                        mazeFieldModel.getCard().typeAndState() + ".png")
                        ).getImage()
                )
        );

        Treasure mazeCardTreasure = mazeFieldModel.getCard().getTreasure();
        if(mazeCardTreasure != null){
            lblCurrentstoneTreasure.setIcon(
                    new ImageIcon(
                            new ImageIcon(
                                    this.getClass().getResource("/img/treasure/Treasure/" + model.getImageSizeVariant() + "/" + mazeCardTreasure.getCode() + ".png")
                            ).getImage()
                    )
            );
        }
        else{
            /* pokud na kameni neni poklad, je treba zobrazit ZADNY poklad
             * Kdyz se posouva poklad s kameny, zpusobovalo to, ze se ikona pokladu nechala na kameni i kdyz byl poklad
             * null. Je treba rici, ze predchozi ikona zde jiz nema byt.
             */
            lblCurrentstoneTreasure.setIcon(null);
        }


        int mazeFieldSize = model.getMazeFieldSize();
        // pocet hracu na policku
        int playersNum = mazeFieldModel.getCard().listPlayers().size();
        int step = mazeFieldSize / (playersNum + 1);
        // na hracim kameni se zobrazi poklad
        int i;
        for(i=0; i < playersNum; i++){
            /*
             * JLabel pro figurky hracu jsou vytvareny az jsou opravdu potreba. Pokud na policku jeste nebyl zadny hrac,
             * nema JLabely pro figurky a lPlayers je prazdny seznam. Ve chvili, kdy na policku je navysen "rekord"
             * obsazenosti, pak je vytvoren JLabel pro figurku.
             *
             */
            Player p = mazeFieldModel.getCard().listPlayers().get(i);
            if(lPlayers.size() > i) { // jiz byl vytvoren JLabel pro hrace, jiz zne byl dany pocet hracu
                lPlayer = lPlayers.get(i);
                lPlayer.setBounds(-(mazeFieldSize / 2) + (i + 1) * step, 0, mazeFieldSize, mazeFieldSize);
                String colorName = p.getColorName();

                lPlayer.setIcon(
                        new ImageIcon(
                                new ImageIcon(
                                        this.getClass().getResource("/img/player/Player/" + model.getImageSizeVariant() + "/figure_" + colorName + ".png")
                                ).getImage()
                        )
                );
            }
            else{ // nyni je zde vice hracu, nez zde zatim bylo, je treba vytvorit dalsi JLabel
                lPlayer = new JLabel();
                lPlayer.setBounds(-(mazeFieldSize / 2) + (i + 1) * step, 0, mazeFieldSize, mazeFieldSize);
                lPlayer.setOpaque(false);
                String colorName = p.getColorName();

                lPlayer.setIcon(
                        new ImageIcon(
                                new ImageIcon(
                                        this.getClass().getResource("/img/player/Player/" + model.getImageSizeVariant() + "/figure_" + colorName + ".png")
                                ).getImage()
                        )
                );
                this.add(lPlayer, new Integer(2));
                lPlayers.add(lPlayer);
            }
        }
        /* Zbyvajici JLabely pro vetsi pocet hracu, nez jaky je aktualne na kameni je treba nastavit na ZADNOU ikonu.
         * Pokud by se tak neprovedlo, zustala by na nich ikona hrace, ktery na danem JLabelu byl naposled i kdyz uz
         * na nem prave ted neni!
         */
        for( ; i < lPlayers.size(); i++){
            lPlayer = lPlayers.get(i);
            lPlayer.setIcon(null);
        }

        button.repaint();
    }
}
