/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */

package labyrinth.view;

import labyrinth.controller.game.ShiftCommand;
import labyrinth.model.Game;
import labyrinth.model.board.MazeField;
import labyrinth.model.player.Player;

import javax.swing.*;
import java.awt.*;

/**
 * Graficka reprezentace hraci desky.
 */
public class GMazeBoard extends JPanel{
    protected Game model;
    protected GameFrame view;
    protected GMazeField[][] fields;

    public GMazeBoard(Game model, GameFrame view){
        this.model = model;
        this.view = view;

        int mazeBoardSize = model.getMB().size();
        this.setLayout(new GridLayout(mazeBoardSize, mazeBoardSize, 0, 0));

        // naplneni desky spravnym poctem poli/kamenu
        fields = new GMazeField[mazeBoardSize+1][mazeBoardSize+1]; // dalo by se predelat na mapovani jako v MazeBoard
        for(int row = 1; row <= mazeBoardSize; row++){
            for(int col = 1; col <= mazeBoardSize; col++){
                // vytvoreni noveho policka
                fields[row][col] = new GMazeField(model, view, row, col);
                // pridani policka do GUI
                this.add(fields[row][col]);
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int mazeBoardSize = model.getMB().size();
        for(int row = 1; row <= mazeBoardSize; row++){
            for(int col = 1; col <= mazeBoardSize; col++) {
                fields[row][col].repaint();
            }
        }
    }


    /**
     * Nastavi policka na okraji na sudych sloupcich necho radcich na aktivni, ostatni neaktivni.
     * Pouziva se ve fazi hry ROTATE_AND_SHIFT.
     */
    public void setActiveShiftableFields(){
        int mazeBoardSize = model.getMB().size();
        for(int row = 1; row <= mazeBoardSize; row++) {
            for (int col = 1; col <= mazeBoardSize; col++) {
                if((col % 2 == 0) && (row == 1 || row == model.getMB().size())
                    || ((row % 2 == 0) && (col == 1 || col == model.getMB().size()))
                ) {
                    fields[row][col].setEnabled(true);
                }
                else {
                    fields[row][col].setEnabled(false);
                }
            }
        }

        // neni mozne vlozit na opacnou stranu oproti poslednimu tahu
        ShiftCommand cmd = view.getInvoker().getLastShiftCommand();
        if(cmd != null){
            MazeField mfLastIns = cmd.getInsertLocation();
            int row = mfLastIns.row();
            int col = mfLastIns.col();
            int n = model.getMB().size();
            if(row == 1 && col%2 == 0){
                fields[n][col].setEnabled(false);
            }
            // posledni radek - kameny se posouvaly nahoru
            else if(row == n && col%2 == 0){
                fields[1][col].setEnabled(false);
            }
            // prvni sloupec - kameny se posouvaly doprava na radku
            else if(col == 1 && row%2 == 0){
                fields[row][n].setEnabled(false);
            }
            // posledni sloupec - kameny se posouvaly doleva
            else if (col == n && row%2 == 0){
                fields[row][1].setEnabled(false);
            }
        }
    }


    /**
     * Nastavi vsechna policka podle zadaneho parametru na aktivni true nebo neaktivni false.
     * @param f urcuje, zda maji byt vsechna policka nastavena na aktivni true, nebo neaktivni false
     */
    public void setEnabledAllFields(boolean f) {
        int mazeBoardSize = model.getMB().size();
        for(int row = 1; row <= mazeBoardSize; row++) {
            for (int col = 1; col <= mazeBoardSize; col++) {
               fields[row][col].setEnabled(f);
            }
        }
    }

    public void setActiveReachableFields() {
        int mazeBoardSize = model.getMB().size();
        for(int row = 1; row <= mazeBoardSize; row++) {
            for (int col = 1; col <= mazeBoardSize; col++) {
                MazeField mf = model.getMB().get(row, col);
                Player p = model.getPP().getCurrentPlayer();
                MazeField playerPosition = model.getMB().whereIs(p);
                boolean reachable = model.getMB().isReachable(playerPosition, mf);
                fields[row][col].setEnabled(reachable);
            }
        }
    }
}
