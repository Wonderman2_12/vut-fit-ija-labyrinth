/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth.view;

import labyrinth.controller.app.CreateNewPlayersAction;
import labyrinth.model.NewGameSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Reprezentuje okno GUI nabidky nove hry.
 * Toto okno se zobrazi pri klinuti na tlacitko New game v hlavni nabidce.
 */
public class NewGameMenuFrame extends JFrame {
    private JFrame frame;


    /**
     * Vytvori okno GUI nabidky nove hry.
     */
    public NewGameMenuFrame() {
        frame = new JFrame("Labyrinth: New game");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 401, 284);
        frame.getContentPane().setLayout(null);
        frame.toFront(); // do popredi

        JLabel lblBoardSize = new JLabel("Board size");
        lblBoardSize.setBounds(94, 38, 81, 26);
        frame.getContentPane().add(lblBoardSize);

        JLabel lblNumberOfTreasures = new JLabel("Number of treasures");
        lblNumberOfTreasures.setBounds(94, 85, 143, 26);
        frame.getContentPane().add(lblNumberOfTreasures);

        JLabel lblNumberOfPlayers = new JLabel("Number of players");
        lblNumberOfPlayers.setBounds(94, 137, 108, 14);
        frame.getContentPane().add(lblNumberOfPlayers);

        JComboBox<String> cbMazeBoardSize = new JComboBox<>();
        String OptionsMazeBoardSize[] = new String[]{"5x5", "7x7", "9x9", "11x11"};
        cbMazeBoardSize.setModel(new DefaultComboBoxModel<>(OptionsMazeBoardSize));
        cbMazeBoardSize.setBounds(229, 40, 69, 23);
        frame.getContentPane().add(cbMazeBoardSize);

        JComboBox<String> cbTreasuresNum = new JComboBox<>();
        String OptionsTreasuresNum[] = new String[]{"12", "24"};
        cbTreasuresNum.setModel(new DefaultComboBoxModel<>(OptionsTreasuresNum));
        cbTreasuresNum.setBounds(229, 87, 69, 23);
        frame.getContentPane().add(cbTreasuresNum);

        JComboBox<String> cbPlayersNum = new JComboBox<>();
        String OptionsPlayersNum[] = new String[]{"2", "3", "4"};
        cbPlayersNum.setModel(new DefaultComboBoxModel<>(OptionsPlayersNum));
        cbPlayersNum.setBounds(229, 133, 69, 23);
        frame.getContentPane().add(cbPlayersNum);

        JButton btnNext= new JButton("Next");
        btnNext.setBounds(94, 184, 204, 26);
        btnNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // vezmeme si zadane hodnoty z comboboxu, pretypujeme vraceny Object na String
                // urcime velikost hraci desky
                int mbSize;
                if("5x5".equals((String)cbMazeBoardSize.getSelectedItem()))
                    mbSize = 5;
                else if("7x7".equals((String)cbMazeBoardSize.getSelectedItem()))
                    mbSize = 7;
                else if("9x9".equals((String)cbMazeBoardSize.getSelectedItem()))
                    mbSize = 9;
                else
                    mbSize = 11;

                // urcime pocet pokladu a hracu
                int trNum = Integer.parseInt((String)cbTreasuresNum.getSelectedItem());
                int plNum = Integer.parseInt((String)cbPlayersNum.getSelectedItem());


                NewGameSettings settings = new NewGameSettings();
                settings.setMazeBoardSize(mbSize);
                settings.setTreasuresNum(trNum);
                settings.setPlayersNum(plNum);
                new CreateNewPlayersAction().execute(frame, settings);
            }
        });
        frame.getContentPane().add(btnNext);

        frame.setResizable(false);

        // zobrazeni okna uprostred obrazovky
        Toolkit t = Toolkit.getDefaultToolkit();
        frame.setLocation(t.getScreenSize().width / 2 - frame.getWidth() / 2,
                t.getScreenSize().height / 2 - frame.getHeight() / 2);

        frame.setVisible(true);
    }
}
