/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */

package labyrinth.view;

import labyrinth.model.Game;
import labyrinth.model.treasure.Treasure;

import javax.swing.*;
import java.awt.*;

/**
 * Graficka reprezentace herniho kamene
 */
public class GFreeMazeCard extends JLayeredPane {
    private Game model;

    private JLabel lCurrentStone;
    private JLabel lCurrentMazeCardTreasure;

    public GFreeMazeCard(Game model) {
        this.model = model;

        this.setOpaque(false);
        this.setPreferredSize(new Dimension(model.getMazeCardSize(), model.getMazeCardSize()));

        // volny hraci kamen - pouze chodba bez pripadneho pokladu
        lCurrentStone = new JLabel();
        lCurrentStone.setBounds(25, 0, model.getMazeCardSize(), model.getMazeCardSize());
        lCurrentStone.setIcon(
                new ImageIcon(
                        new ImageIcon(
                                this.getClass().getResource("/img/board/MazeCard/" + model.getImageSizeVariant() + "/" +
                                        model.getMB().getFreeCard().typeAndState() + ".png")
                        ).getImage()
                )
        );
        lCurrentStone.setOpaque(true);
        this.add(lCurrentStone, new Integer(0));

        // na hracim kameni se zobrazi poklad
        Treasure mazeCardTreasure = model.getMB().getFreeCard().getTreasure();
        lCurrentMazeCardTreasure = new JLabel("");
        lCurrentMazeCardTreasure.setBounds(25, 0, model.getMazeCardSize(), model.getMazeCardSize());
        lCurrentMazeCardTreasure.setOpaque(false);
        if(mazeCardTreasure != null){
            lCurrentMazeCardTreasure.setIcon(
                    new ImageIcon(
                            new ImageIcon(
                                    this.getClass().getResource("/img/treasure/Treasure/" + model.getImageSizeVariant() + "/" + mazeCardTreasure.getCode() + ".png")
                            ).getImage()
                    )
            );
        }
        else{
            /* pokud na kameni neni poklad, je treba zobrazit ZADNY poklad
             * Kdyz se posouva poklad s kameny, zpusobovalo to, ze se ikona pokladu nechala na kameni i kdyz byl poklad
             * null. Je treba rici, ze predchozi ikona zde jiz nema byt.
             */
            lCurrentMazeCardTreasure.setIcon(null);
        }
        this.add(lCurrentMazeCardTreasure, new Integer(1));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        lCurrentStone.setIcon(
                new ImageIcon(
                        new ImageIcon(
                                this.getClass().getResource("/img/board/MazeCard/" + model.getImageSizeVariant() + "/" +
                                        model.getMB().getFreeCard().typeAndState() + ".png")
                        ).getImage()
                )
        );

        Treasure mazeCardTreasure = model.getMB().getFreeCard().getTreasure();
        if(mazeCardTreasure != null){
            lCurrentMazeCardTreasure.setIcon(
                    new ImageIcon(
                            new ImageIcon(
                                    this.getClass().getResource("/img/treasure/Treasure/" + model.getImageSizeVariant() + "/" + mazeCardTreasure.getCode() + ".png")
                            ).getImage()
                    )
            );
        }
        else{
            /* pokud na kameni neni poklad, je treba zobrazit ZADNY poklad
             * Kdyz se posouva poklad s kameny, zpusobovalo to, ze se ikona pokladu nechala na kameni i kdyz byl poklad
             * null. Je treba rici, ze predchozi ikona zde jiz nema byt.
             */
            lCurrentMazeCardTreasure.setIcon(null);
        }

    }
}
