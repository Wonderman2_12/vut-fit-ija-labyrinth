/** VUT FIT IJA
 * Labyrinth
 * grp274
 * Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
 * Martin Vondracek <xvondr20@stud.fit.vutbr.cz>
 * 8. 5. 2015
 */


package labyrinth;


import labyrinth.view.MainMenuFrame;

import javax.swing.*;

/**
 * Hlavni trida aplikace.
 * Slouzi ke spusteni GUI.
 */
public class Labyrinth {
    /** GUI pro hlavni nabidku */
    private MainMenuFrame mainMenuFrame;

    /**
     * Hlavni metoda aplikace.
     * Zajistuje spusteni GUI.
     * @param argv parametry programu
     */
    public static void main(String argv[]) {
        javax.swing.SwingUtilities.invokeLater(new Runnable () {

            @Override
            public void run(){
                createAndShowGUI();
            }

        });
    }


    /**
     * Vytvori a zobrazi GUI.
     * Nastavuje <code>JFrame.setDefaultLookAndFeelDecorated(true)</code>.
     */
    private static void createAndShowGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        new Labyrinth();
    }


    /**
     * Zada vytvoreni GUI pro hlavni nabidku.
     */
    public Labyrinth(){
        mainMenuFrame = new MainMenuFrame();
    }
}
