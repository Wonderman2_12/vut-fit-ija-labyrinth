#!/bin/sh
# Stazeni pozadovanych externich knihoven a souboru
# grp274
# Dita Cihlarova <xcihla02@stud.fit.vutbr.cz>
# Martin Vondracek <xvondr20@stud.fit.vutbr.cz>

wget http://www.stud.fit.vutbr.cz/~xvondr20/IJA-grp274-img.zip
unzip IJA-grp274-img.zip
rm IJA-grp274-img.zip
